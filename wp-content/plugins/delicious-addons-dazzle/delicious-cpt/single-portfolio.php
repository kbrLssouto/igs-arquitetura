<?php get_header(); ?>
	
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
	<?php get_template_part( 'inc/page-title' ); ?>
	

		<section class="portfolio-single container">
			<article id="post-<?php esc_attr(the_ID()); ?>" class="begin-content">

			<div class="clear"></div>

			<?php the_content(); ?>		
		
			</article>
		</section>
		
		<div class="clear"></div>
	
		<?php do_action('delicious_project_footer'); ?>

	<?php endwhile; endif; ?>	

<?php get_footer(); ?>