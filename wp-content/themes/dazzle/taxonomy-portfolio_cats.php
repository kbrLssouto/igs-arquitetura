<?php
	get_header(); 

$dazzle_data = dazzle_dt_data();	
?>

	<div class="page-title-wrapper">
		<div class="container">
			<h2 class="section-title"><?php _e('Category: ', 'dazzle'); ?><strong><?php single_cat_title(); ?></strong></h2>
			<?php
				$categ_desc = category_description( get_the_category( $id ) ); 

				if($categ_desc != '') { 
				?>
					<h4 class="section-tagline"><?php echo esc_html($categ_desc) ?> </h4>
				<?php } ?>			
		</div>
	</div>
	<div class="space"></div>

	<div class="container">
	<section class="delicious-grid" id="gridwrapper_portfolio">		

		<section id="portfolio-wrapper">		
			<ul class="portfolio grid three-cols dt-gap-15 isotope dt-gallery grid_portfolio">
			
				<?php

				// Begin The Loop
				if (have_posts()) : while (have_posts()) : the_post(); 	

				// Get The Taxonomy 'Filter' Categories
				$dazzle_terms = get_the_terms( get_the_ID(), 'portfolio_cats' ); 

				$dazzle_portf_icon = get_post_meta($post->ID,'dazzle_portf_icon',true);						
				$dazzle_portf_link = get_post_meta($post->ID,'dazzle_portf_link',true);						
				$portf_video = get_post_meta($post->ID,'dazzle_portf_video',true);						
				$dazzle_portf_thumbnail = get_post_meta($post->ID,'dazzle_portf_thumbnail',true);	
				
				$dazzle_lgal = rwmb_meta( 'dazzle_portf_gallery', 'type=image_advanced&size=full', $post->ID );

				$dazzle_gal_output = '';
				if(!empty($dazzle_lgal)) { 
					$dazzle_gal_output .= '<div class="dt-single-gallery">';
					
					foreach($dazzle_lgal as $dazzle_gal_item) {
						$dazzle_gal_output .= '<a href="'.esc_url($dazzle_gal_item['url']).'" title="'.esc_attr($dazzle_gal_item['title']).'"></a>';
					}
					$dazzle_gal_output .= '</div>';
				}

				$dazzle_thumb_id = get_post_thumbnail_id($post->ID);
				$dazzle_image_url = wp_get_attachment_url($dazzle_thumb_id);
				
				$dazzle_grid_thumbnail = $dazzle_image_url;
				$dazzle_item_class = 'item-small';
				
				switch ($dazzle_portf_thumbnail) {
					case 'landscape':
						$dazzle_grid_thumbnail = aq_resize($dazzle_image_url, 640, 480, true);
						$dazzle_item_class = 'item-wide';
						break;
					case 'portrait':
						$dazzle_grid_thumbnail = aq_resize($dazzle_image_url, 640, 853, true);
						$dazzle_item_class = 'item-small';
						break;								
				}				
					
				
				?>
				<li class="grid-item">
					<?php	
						if ($dazzle_portf_icon == 'lightbox_to_image') { ?>
							<a href="<?php echo esc_url(wp_get_attachment_url($dazzle_thumb_id));?>" class="img-anchor dt-lightbox-gallery" title="<?php esc_attr(the_title()); ?>">
								<div class="project-hover">
									<i class="fa fa-search"></i>
								</div>
								<img src="<?php echo esc_url($dazzle_grid_thumbnail); ?>" alt="" />
							</a>
						<?php } 
						else if ($dazzle_portf_icon == 'link_to_page') {  ?>
							<a class="img-anchor" href="<?php esc_url(the_permalink()); ?>">
								<div class="project-hover">
									<i class="fa fa-external-link"></i>
								</div>								
								<img src="<?php echo esc_url($dazzle_grid_thumbnail); ?>" alt="" />
							</a>
						<?php } 
						else if ($dazzle_portf_icon == 'link_to_link') {  ?>
							<a class="img-anchor" href='<?php echo esc_url($dazzle_portf_link); ?>'>
								<div class="project-hover">
									<i class="fa fa-external-link"></i>
								</div>								
								<img src="<?php echo esc_url($dazzle_grid_thumbnail); ?>" alt="" />
							</a>
						<?php }	
						else if ($dazzle_portf_icon == 'lightbox_to_video') {  ?>
							<a class="img-anchor dt-lightbox-gallery mfp-iframe" href="<?php echo esc_url($portf_video); ?>" title="<?php esc_attr(the_title()); ?>">
								<div class="project-hover">
									<i class="fa fa-play-circle"></i>
								</div>								
								<img src="<?php echo esc_url($dazzle_grid_thumbnail); ?>" alt="" />
							</a>
						<?php }	
						if ($dazzle_portf_icon == 'lightbox_to_gallery') {  ?> 
							<a class="dt-gallery-trigger img-anchor" title="<?php esc_attr(the_title()); ?>" >
								<div class="project-hover">
									<i class="fa fa-picture-o"></i>
								</div>								
								<img src="<?php echo esc_url($dazzle_grid_thumbnail); ?>" alt="" />
							</a>
						<?php echo $dazzle_gal_output; } ?>

						<div class="grid-item-on-hover">
							<div class="grid-text">
								<h3><a href="<?php esc_url(the_permalink()); ?>"><?php echo esc_html(get_the_title()); ?></a></h3>
								<div class="grid-item-cat">
								<?php
								$dazzle_copy = $dazzle_terms;
								if(isset($dazzle_data['dazzle_taxonomy_links']) && ($dazzle_data['dazzle_taxonomy_links'] =='1')) {
									foreach ( $dazzle_terms as $dazzle_term ) {
										$dazzle_term_link = get_term_link($dazzle_term->slug, 'portfolio_cats');
										if (function_exists('icl_t')) { 
										   echo '<a href="'.esc_url($dazzle_term_link).'">'.icl_t('Portfolio Category', 'Term '.delicious_get_taxonomy_cat_ID( $dazzle_term->name ).'', $dazzle_term->name).'</a>'; 
										}
										else 
											echo '<a href="'.esc_url($dazzle_term_link).'">'.esc_html($dazzle_term->name).'</a>';
											if (next($dazzle_copy )) {
												echo esc_html__(', ', 'dazzle');
											}
										}	
								}
								else {
									foreach ( $dazzle_terms as $dazzle_term ) {
									if (function_exists('icl_t')) { 
									   echo icl_t('Portfolio Category', 'Term '.delicious_get_taxonomy_cat_ID( $dazzle_term->name ).'', $dazzle_term->name);
									}
									else 
										echo esc_html($dazzle_term->name);
										if (next($dazzle_copy )) {
											echo esc_html__(', ', 'dazzle');
										}
									}										
								}

								?>
								</div>									
							</div>

						</div>							
						

				</li>

	
				<?php endwhile; endif; // END the Wordpress Loop ?>
			</ul>
			<?php dazzle_navigation(); ?>
			<?php wp_reset_postdata(); // Reset the Query Loop ?>			
					
		</section>
	</section>
	</div><!--end centered-wrapper-->
	<div class="space"></div>

<?php get_footer(); ?>