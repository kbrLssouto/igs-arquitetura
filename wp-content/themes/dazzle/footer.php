<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Dazzle Theme
 */

?>
<?php $dazzle_data = dazzle_dt_data(); ?>

	</div><!-- #content -->

	<?php  
		$dazzle_fclass = '';
		if(isset($dazzle_data['dazzle_footer_layout'])) {
				$dazzle_fclass = $dazzle_data['dazzle_footer_layout'];
		} 
	?>	

	<footer id="colophon" class="site-footer <?php echo esc_attr($dazzle_fclass); ?>">

		
		<?php
		if ( is_active_sidebar( 'footer' ) ) : ?>	
		<div class="container">	
			<div id="topfooter">
				<?php dynamic_sidebar( 'footer' ); ?>
			</div><!--end topfooter-->
			
		</div><!--end container-->
		<?php endif; ?>	


		<div class="container">
			<?php 
			if(isset($dazzle_data['dazzle_svg_footer_enabled']) && ($dazzle_data['dazzle_svg_footer_enabled'] == '1')) { 
					if(isset($dazzle_data['dazzle_svg_footer_logo']['url']) && ($dazzle_data['dazzle_svg_footer_logo']['url'] !='')) {
					?>
				<div class="footer-logo">
					<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php esc_attr(bloginfo( 'name' )); ?>" rel="home"><img class="is-svg" src="<?php echo esc_url($dazzle_data['dazzle_svg_footer_logo']['url']); ?>" alt="<?php esc_attr(bloginfo( 'name' )) ?>" width="<?php echo esc_attr($dazzle_data['dazzle_svg_footer_logo_width']); ?>" height="<?php echo esc_attr($dazzle_data['dazzle_svg_footer_logo_height']); ?>" /></a>
				</div>
			<?php	
			} }
			else if(isset($dazzle_data['dazzle_footer_logo']['url']) && ($dazzle_data['dazzle_footer_logo']['url'] !='')) { ?>
				<div class="footer-logo">
					<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php esc_attr(bloginfo( 'name' )); ?>" rel="home"><img src="<?php echo esc_url($dazzle_data['dazzle_footer_logo']['url']); ?>" alt="<?php esc_attr(bloginfo( 'name' )) ?>" /></a>
				</div>
			<?php } ?>		

			<ul id="social" class="align-center">
				<?php
					$dazzle_social_links = array('rss','facebook','twitter','flickr','google-plus', 'dribbble' , 'linkedin', 'pinterest', 'youtube', 'github-alt', 'vimeo-square', 'instagram', 'tumblr', 'behance', 'vk', 'xing', 'soundcloud', 'codepen', 'yelp', 'slideshare', 'houzz', '500px');
					if($dazzle_social_links) {
						foreach($dazzle_social_links as $dazzle_social_link) {
							if(!empty($dazzle_data[$dazzle_social_link])) { echo '<li><a href="'. esc_url($dazzle_data[$dazzle_social_link]) .'" title="'. esc_attr($dazzle_social_link) .'" class="'.esc_attr($dazzle_social_link).'"  target="_blank"><i class="fa fa-'.esc_attr($dazzle_social_link).'"></i></a></li>';
							}								
						}
						if(!empty($dazzle_data['skype'])) { echo '<li><a href="skype:'. esc_attr($dazzle_data['skype']) .'?call" title="'. esc_attr($dazzle_data['skype']) .'" class="'.esc_attr($dazzle_data['skype']).'"  target="_blank"><i class="fa fa-skype"></i></a></li>';
						}							
					}
				?>
			</ul>
			<div class="site-info">
				<?php if(isset($dazzle_data['dazzle_copyright_textarea']) && ($dazzle_data['dazzle_copyright_textarea'] !='')) { 
					echo wp_kses_post($dazzle_data['dazzle_copyright_textarea']);
				 		} else {  
				 	esc_html_e('Copyright - Dazzle | All Rights Reserved', 'dazzle'); 
				 } ?>
			</div><!-- .site-info -->
		</div>
	</footer><!-- #colophon -->

	<a class="upbtn" href="#">
		<svg class="arrow-top" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="25 25 50 50" enable-background="new 0 0 100 100" xml:space="preserve"><g><path d="M42.8,47.5c0.4,0.4,1,0.4,1.4,0l4.8-4.8v21.9c0,0.6,0.4,1,1,1s1-0.4,1-1V42.7l4.8,4.8c0.4,0.4,1,0.4,1.4,0   c0.4-0.4,0.4-1,0-1.4L50,38.9l-7.2,7.2C42.4,46.5,42.4,47.1,42.8,47.5z"/></g></svg>
	</a>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
