<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package Dazzle Theme
 */

if ( ! is_active_sidebar( 'sidebar' ) ) {
	return;
}
?>

<div id="secondary" class="widget-area percent-sidebar">
		<?php	
		$dazzle_sidebars = get_post_meta($post->ID, 'dazzle_all_sidebars', true);
		if(!empty($dazzle_sidebars)) { 
				dynamic_sidebar( $dazzle_sidebars );
		}

		else {
			if ( is_active_sidebar( 'sidebar' ) ) { 
				dynamic_sidebar( 'sidebar' ); 
				}		
			}
		?>
</div><!-- #secondary -->
