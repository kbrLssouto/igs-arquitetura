<?php
/**
 * Template part for displaying posts.
 *
 * @package Dazzle Theme
 */

?>

<article id="post-<?php esc_attr(the_ID()); ?>" <?php esc_attr(post_class()); ?>>

	<?php 
		$dazzle_video_class = new dazzle_Delicious;
		$dazzle_video_class->dazzle_video($post->ID); 
	 ?>

	<header class="entry-header">

		<?php if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php if(!is_single()) { ?>
				<?php dazzle_posted_on(); ?>
			<?php } ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
				
		<?php if(!is_single()) { ?>
			<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
		<?php } ?>

	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php

			dazzle_more_tag();
			the_content(esc_html__('Continue Reading', 'dazzle'));
		
		?>

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'dazzle' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php dazzle_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
