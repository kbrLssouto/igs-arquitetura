<?php
/**
 * Template part for displaying posts.
 *
 * @package Dazzle Theme
 */

?>

<?php

	$dazzle_thumb_id = get_post_thumbnail_id($post->ID);
	$dazzle_alt = get_post_meta($dazzle_thumb_id, '_wp_attachment_image_alt', true);

?>

<article id="post-<?php esc_attr(the_ID()); ?>" <?php esc_attr(post_class('grid-item')); ?>>


	<?php
	if(has_post_format('quote', $post->ID)) {

		$dazzle_quote_post_text = get_post_meta($post->ID, 'dazzle_quote_text', true);
		$dazzle_quote_post_author = get_post_meta($post->ID, 'dazzle_quote_author', true);
		?>
		<div class="quote-box">
			<p><?php echo esc_html($dazzle_quote_post_text);?></p>
			<span><?php echo esc_html($dazzle_quote_post_author);?></span>
		</div>		
	<?php } else { 

	if ( 'post' === get_post_type() ) :
		if ( has_post_thumbnail() ) { ?>
				<div class="post-thumbnail">
					<a href="<?php esc_url(the_permalink()); ?>">
						<?php 
						the_post_thumbnail('dazzle-blog-grid-thumbnail', array('alt'   => $dazzle_alt)); 
						?>										
					</a>
				</div><!--end post-thumbnail-->		
			<?php } ?>
		<header class="entry-header">
			<div class="entry-meta">
				<?php
					$dazzle_time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
					if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
						$dazzle_time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
					}

					$dazzle_time_string = sprintf( $dazzle_time_string,
						esc_attr( get_the_date( 'c' ) ),
						esc_html( get_the_date() ),
						esc_attr( get_the_modified_date( 'c' ) ),
						esc_html( get_the_modified_date() )
					);

					echo '<span class="posted-on">' . $dazzle_time_string . '</span>';

				?>
			</div><!-- .entry-meta -->
			<?php endif; ?>
					
			<?php the_title( sprintf( '<h3 class="entry-title"><a href="%s" rel="bookmark"><span>', esc_url( get_permalink() ) ), '</span></a></h3>' ); ?>

		</header><!-- .entry-header -->

		<?php
			$dazzle_categories_list = get_the_category_list( esc_html__( ' ', 'dazzle' ) );
			if ( $dazzle_categories_list && dazzle_categorized_blog() ) {
				printf( '<span class="cat-links">' . esc_html__( '%1$s', 'dazzle' ) . '</span>', $dazzle_categories_list );
			}
		?>	

		<footer class="entry-footer">
			<?php dazzle_entry_footer(); ?>
		</footer><!-- .entry-footer -->
	<?php } ?>
</article>