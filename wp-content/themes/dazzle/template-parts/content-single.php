<?php
/**
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Dazzle Theme
 */

?>


<?php

	$dazzle_thumb_id = get_post_thumbnail_id($post->ID);
	$dazzle_alt = get_post_meta($dazzle_thumb_id, '_wp_attachment_image_alt', true);

?>

<article id="post-<?php esc_attr(the_ID()); ?>" <?php esc_attr(post_class()); ?>>

	<div class="post-thumbnail">
		<?php the_post_thumbnail('dazzle-blog-thumbnail', array('alt'   => $dazzle_alt)); ?>
	</div><!--end post-thumbnail-->		

	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		<div class="entry-meta">
			<?php dazzle_posted_on(); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'dazzle' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php dazzle_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

