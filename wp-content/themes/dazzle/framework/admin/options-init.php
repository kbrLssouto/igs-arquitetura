<?php

    /**
     * For full documentation, please visit: http://docs.reduxframework.com/
     * For a more extensive sample-config file, you may look at:
     * https://github.com/reduxframework/redux-framework/blob/master/sample/sample-config.php
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "dazzle_redux_data";

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        'opt_name' => 'dazzle_redux_data',
        'use_cdn' => TRUE,
        'page_slug' => 'delicious_options',
        'page_title' => esc_html__('Delicious Options', 'dazzle'),
        'update_notice' => FALSE,
        'dev_mode' => FALSE, //SET TO FALSE
        'display_name' => $theme->get('Name'),
        'display_version' => $theme->get('Version'),
        'admin_bar' => TRUE,
        'menu_type' => 'submenu',
        'menu_title' => esc_html__('Delicious Options', 'dazzle'),
        'allow_sub_menu' => TRUE,
        'customizer' => TRUE,
        'default_mark' => '*',
        'google_api_key' => 'AIzaSyBPVwg6CaFLmKlxYjQu0bJGpxDN1p04S-Q',
        'disable_tracking' => TRUE,
        'hints' => array(
            'icon' => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color' => 'lightgray',
            'icon_size' => 'normal',
            'tip_style' => array(
                'color' => 'light',
            ),
            'tip_position' => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect' => array(
                'show' => array(
                    'duration' => '500',
                    'event' => 'mouseover',
                ),
                'hide' => array(
                    'effect' => 'fade',
                    'duration' => '500',
                    'event' => 'mouseleave unfocus',
                ),
            ),
        ),
        'output' => TRUE,
        'output_tag' => TRUE,
        'settings_api' => TRUE,
        'cdn_check_time' => '1440',
        'compiler' => TRUE,
        'page_permissions' => 'manage_options',
        'save_defaults' => TRUE,
        'show_import_export' => TRUE,
        'database' => 'options',
        'transient_time' => '3600',
        'network_sites' => TRUE,
    );

    // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
    $args['share_icons'][] = array(
        'url'   => 'http://themeforest.net/user/deliciousthemes/portfolio',
        'title' => esc_html__( 'Check out our Portfolio', 'dazzle' ),
        'icon'  => 'el el-globe-alt'
    );
    $args['share_icons'][] = array(
        'url'   => 'https://www.facebook.com/deliciousthemes',
        'title' => esc_html__( 'Like us on Facebook', 'dazzle' ),
        'icon'  => 'el el-facebook'
    );
    $args['share_icons'][] = array(
        'url'   => 'http://twitter.com/deliciousthemes',
        'title' => esc_html__( 'Follow us on Twitter', 'dazzle' ),
        'icon'  => 'el el-twitter'
    );


    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */

    /*
     *
     * ---> START SECTIONS
     *
     */
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'General', 'dazzle' ),
        'id'         => 'general',
        'icon'  => 'el el-globe-alt',
        'fields'     => array(
                    array(
                        'id'        => 'opt-info-field',
                        'type'      => 'info',
                        'title'  => esc_html__('Welcome to Dazzle Options Panel.', 'dazzle'),
                        'desc'      => esc_html__('It is meant to make your life easier by offering you options for customizing your website (upload custom logo, choose a color scheme, set up footer social icons, etc.).', 'dazzle')
                    ),      
 
                    array(
                        'id'        => 'dazzle_main_layout',
                        'type'      => 'image_select',
                        'title'     => esc_html__('Layout', 'dazzle'),
                        'desc'  => esc_html__('Select the main content alignment. Choose between wide and boxed layout.', 'dazzle'),
                        'options'   => array(
                            'wide-layout' => array('alt' => 'Wide Layout',  'img' => ReduxFramework::$_url . 'assets/img/1c.png'),
                            'boxed-layout' => array('alt' => 'Boxed Layout',  'img' => ReduxFramework::$_url . 'assets/img/3cm.png')
                        ),
                        'default'   => 'wide-layout'
                    ),    
                    array(
                        'id'        => 'dazzle_enable_preloader',
                        'type'      => 'switch',
                        'title'     => esc_html__('Website Preloader', 'dazzle'),
                        'subtitle'  => esc_html__('You can enable/disable the website`s spinning wheel preloader.', 'dazzle'),
                        'default'   => 1,
                        'on'        => 'On',
                        'off'       => 'Off'
                    ),  

                   array(
                        'id'        => 'section-preloader-start',
                        'type'      => 'section',
                        'indent'    => true, 
                        'required'  => array('dazzle_enable_preloader', '=', '1'),
                    ),                    
                   array(
                        'id'        => 'dazzle_preloader_logo',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => esc_html__('Preloader Logo(Optional)', 'dazzle'),
                        'desc'  => esc_html__('You can insert a logo in the middle of the preloader. Tip: image should be circle shaped.', 'dazzle')             
                    ),
                    array(
                        'id'        => 'section-preloader-end',
                        'type'      => 'section',
                        'indent'    => false,
                        'required'  => array('dazzle_enable_preloader', "=", 1),
                    ),                      
                    array(
                        'id'        => 'dazzle_grayscale_effect',
                        'type'      => 'switch',
                        'title'     => esc_html__('Grayscale(black&white) Effect', 'dazzle'),
                        'subtitle'  => esc_html__('You can enable/disable the website`s grayscale effect for the images.', 'dazzle'),
                        'default'   => 1,
                        'on'        => 'On',
                        'off'       => 'Off'
                    ),                                                           
                    array(
                        'id'        => 'dazzle_site_desc_enabled',
                        'type'      => 'switch',
                        'title'     => esc_html__('WordPress Site Tagline', 'dazzle'),
                        'desc'  => esc_html__('Enable/Disable the WordPress site tagline near logo. You can set a tagline in Settings->General.', 'dazzle'),
                        'default'   => 0,
                        'on'        => 'On',
                        'off'       => 'Off'
                    ),      
                    array(
                        'id'        => 'dazzle_smoothscroll_enabled',
                        'type'      => 'switch',
                        'title'     => esc_html__('Smooth Scrolling Effect', 'dazzle'),
                        'subtitle'  => esc_html__('Enable/Disable the Smooth Scrolling effect for the website.', 'dazzle'),
                        'default'   => 1,
                        'on'        => 'On',
                        'off'       => 'Off'
                    ),  
                    array(
                        'id'        => 'dazzle_breadcrumbs_enabled',
                        'type'      => 'switch',
                        'title'     => esc_html__('Breadcrumbs', 'dazzle'),
                        'subtitle'  => esc_html__('Enable/Disable breadcrumbs. Breadcrumbs are a type of navigation which appears under a page title).', 'dazzle'),
                        'default'   => 0,
                        'on'        => 'On',
                        'off'       => 'Off'
                    ),    
                   array(
                        'id'        => 'section-breadcrumbs-start',
                        'type'      => 'section',
                        'indent'    => true, 
                        'required'  => array('dazzle_breadcrumbs_enabled', '=', '1'),
                    ),                    
                    array(
                        'id'       => 'dazzle_breadcrumbs_for',
                        'type'     => 'checkbox',
                        'title'    => esc_html__('Enable Breadcrumbs for:', 'dazzle'), 
                        'desc'     => esc_html__('Select on what types of pages to display breadcrumbs.', 'dazzle'),
                        'options'  => array(
                            'pages' => 'Pages',                            
                            'posts' => 'Posts',
                            'projects' => 'Projects'
                        ),
                        'default' => array(
                            'pages' => '1', 
                            'posts' => '0', 
                            'projects' => '0'
                        )
                    ),
                    array(
                        'id'        => 'section-breadcrumbs-end',
                        'type'      => 'section',
                        'indent'    => false,
                        'required'  => array('dazzle_breadcrumbs_enabled', "=", 1),
                    ),                       


        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Logo', 'dazzle' ),
        'id'         => 'logo',
        'icon'  => 'el el-picasa',
        'fields'     => array(
                   array(
                        'id'        => 'dazzle_custom_logo',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => esc_html__('Main Logo', 'dazzle'),
                        'desc'  => esc_html__('Upload an image that will represent your website`s logo.', 'dazzle'),
                        'default'   => array('url' => 'https://deliciousthemes.com/logo-dazzle.png')                
                    ),

                    array(
                        'id'        => 'dazzle_svg_enabled',
                        'type'      => 'switch',
                        'title'     => esc_html__('Use SVG Logo', 'dazzle'),
                        'desc'  => esc_html__('You can use an .svg logo instead of a regular .png or .jpg logo.', 'dazzle'),
                        'default'   => 0,
                        'on'        => 'Yes',
                        'off'       => 'No',
                    ),  
                   array(
                        'id'        => 'dazzle_section-svglogo-start',
                        'type'      => 'section',
                        'indent'    => true, // Indent all options below until the next 'section' option is set.
                        'required'  => array('dazzle_svg_enabled', '=', '1'),
                    ),

                    array(
                        'id'        => 'dazzle_svg_logo',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => esc_html__('Upload an SVG Logo', 'dazzle'),
                        //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                        'desc'  => esc_html__('Upload your SVG logo. Make sure to set the width and height in the next fields.', 'dazzle'),
                        'default'   => ''                
                    ),  

                    array(
                        'id'        => 'dazzle_svg_logo_width',
                        'type'      => 'text',
                        'title'     => esc_html__('SVG Logo Width', 'dazzle'),
                        'desc'  => esc_html__('If you enter 100, the logo width will be set to 100px. ', 'dazzle'),
                        'desc'      => esc_html__('Use numbers only', 'dazzle'),
                        'validate'  => 'numeric',
                        'default'   => '85'
                    ),        

                    array(
                        'id'        => 'dazzle_svg_logo_height',
                        'type'      => 'text',
                        'title'     => esc_html__('SVG Logo Height', 'dazzle'),
                        'desc'  => esc_html__('If you enter 50, the logo height will be set to 50px. ', 'dazzle'),
                        'desc'      => esc_html__('Use numbers only', 'dazzle'),
                        'validate'  => 'numeric',
                        'default'   => '25'
                    ),                                            

                    array(
                        'id'        => 'section-svglogo-end',
                        'type'      => 'section',
                        'indent'    => false, // Indent all options below until the next 'section' option is set.
                        'required'  => array('dazzle_svg_enabled', "=", 1),
                    ),                        

                    array(
                        'id'        => 'dazzle_alternativelogo_enabled',
                        'type'      => 'switch',
                        'title'     => esc_html__('Alternative Logo', 'dazzle'),
                        'desc'  => esc_html__('You can choose to display an alternative logo for the scrolling state of the header(when header is scrolled down). Make sure to have the Scrolling Effect enabled in the Theme Options->Header section.', 'dazzle'),
                        'default'   => 0,
                        'on'        => 'Yes',
                        'off'       => 'No',
                    ),  
                   array(
                        'id'        => 'section-alternativelogo-start',
                        'type'      => 'section',
                        'indent'    => true, // Indent all options below until the next 'section' option is set.
                        'required'  => array('dazzle_alternativelogo_enabled', '=', '1'),
                    ),

                    array(
                        'id'        => 'dazzle_alternative_logo',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => esc_html__('Upload Alternative Logo', 'dazzle'),
                        //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                        'desc'  => esc_html__('You can upload an alternative logo for the scrolling state of the header.', 'dazzle'),
                        'required'  => array('dazzle_alternativelogo_enabled', '=', '1'),
                        'default'   => ''                
                    ),  

                   array(
                        'id'        => 'dazzle_alternative_svg_enabled',
                        'type'      => 'switch',
                        'title'     => esc_html__('Use Alternative SVG Logo', 'dazzle'),
                        'desc'  => esc_html__('You can use an .svg logo instead of a regular .png or .jpg logo.', 'dazzle'),
                        'default'   => 0,
                        'on'        => 'Yes',
                        'off'       => 'No',
                    ),  
                   array(
                        'id'        => 'dazzle_section-alternative-svglogo-start',
                        'type'      => 'section',
                        'indent'    => true, // Indent all options below until the next 'section' option is set.
                        'required'  => array('dazzle_alternative_svg_enabled', '=', '1'),
                    ),

                    array(
                        'id'        => 'dazzle_alternative_svg_logo',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => esc_html__('Upload an Alternative SVG Logo', 'dazzle'),
                        //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                        'desc'  => esc_html__('Upload your SVG logo. Make sure to set the width and height in the next fields.', 'dazzle'),
                        'default'   => ''                
                    ),  

                    array(
                        'id'        => 'dazzle_alternative_svg_logo_width',
                        'type'      => 'text',
                        'title'     => esc_html__('Alternative SVG Logo Width', 'dazzle'),
                        'desc'  => esc_html__('If you enter 100, the logo width will be set to 100px. ', 'dazzle'),
                        'desc'      => esc_html__('Use numbers only', 'dazzle'),
                        'validate'  => 'numeric',
                        'default'   => '85'
                    ),        

                    array(
                        'id'        => 'dazzle_alternative_svg_logo_height',
                        'type'      => 'text',
                        'title'     => esc_html__('Alternative SVG Logo Height', 'dazzle'),
                        'desc'  => esc_html__('If you enter 50, the logo height will be set to 50px. ', 'dazzle'),
                        'desc'      => esc_html__('Use numbers only', 'dazzle'),
                        'validate'  => 'numeric',
                        'default'   => '25'
                    ),                                            

                    array(
                        'id'        => 'section-alternative-svglogo-end',
                        'type'      => 'section',
                        'indent'    => false, // Indent all options below until the next 'section' option is set.
                        'required'  => array('dazzle_alternative_svg_enabled', "=", 1),
                    ),                                                                         

                    array(
                        'id'        => 'section-alternativelogo-end',
                        'type'      => 'section',
                        'indent'    => false, // Indent all options below until the next 'section' option is set.
                        'required'  => array('dazzle_alternativelogo_enabled', '=', '1'),
                    ),         

                    array(
                        'id'        => 'dazzle_margin_logo',
                        'type'      => 'slider',
                        'title'     => esc_html__('Margin-Top Value for Header`s Logo', 'dazzle'),
                        'desc'  => esc_html__('You can adjust the logo position in header by setting a top-margin to it. You can use negative values as well. For example, if you enter 10, the logo will be lowered by 10px. ', 'dazzle'),
                        'desc'      => esc_html__('Use numbers only', 'dazzle'),
                        'default'       => 0,
                        'min'           => -100,
                        'step'          => 1,
                        'max'           => 100,
                        'display_value' => 'text'                           
                    ),   

                    array(
                        'id'        => 'dazzle_onscroll_logo_height',
                        'type'      => 'slider',
                        'title'     => esc_html__('Logo Height on Scroll', 'dazzle'),
                        'desc'  => esc_html__('Adjust the logo height on scroll. Currently is set to 25px', 'dazzle'),
                        'desc'      => esc_html__('Use numbers only', 'dazzle'),
                        'default'       => 25,
                        'min'           => 1,
                        'step'          => 1,
                        'max'           => 200,
                        'display_value' => 'text'                           
                    )                                      

        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Header', 'dazzle' ),
        'icon'       => 'el el-icon-star-empty',
        'id'         => 'admin-header',
        ));

    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'General Settings', 'dazzle' ),
        'id'         => 'admin-header-general',     
        'subsection'       => true,   
        'fields'     => array(
                    array(
                        'id'        => 'dazzle_header_type',
                        'type'      => 'button_set',
                        'title'     => esc_html__('Header Type', 'dazzle'),
                        'subtitle'  => esc_html__('Set the type of the header: regular top header or left side header.', 'dazzle'),
                        
                        //Must provide key => value pairs for radio options
                        'options'   => array(
                            'header-regular' => 'Regular Top Header', 
                            'header-left' => 'Left Side Header'
                        ), 
                        'default'   => 'header-regular'
                    ),             

                    array(
                        'id'        => 'dazzle_nav_hashtags',
                        'type'      => 'switch',
                        'title'     => __('Enable/Disable Hashtags in URL', 'dazzle'),
                        'subtitle'  => __('You can enable hashtags in the URL of your webiste when clicking on menu items to navigate on page.', 'dazzle'),
                        'default'   => false,
                        'on'        => 'Enabled',
                        'off'       => 'Disabled'
                    ),                        

                    array(
                        'id'        => 'dazzle_scrolloffset',
                        'type'      => 'text',
                        'title'     => __('Navigation ScrollOffset Value', 'dazzle'),
                        'subtitle'  => __('You can adjust the position at which the scrolling effect stops when a menu item is clicked. You can use it to set an offset value to the top of each section stop. For example, the 100 value will stop the navigation 100px above the section.', 'dazzle'),
                        'desc'      => __('Use numbers only', 'dazzle'),
                        'validate'  => 'numeric',
                        'default'   => '0'
                    ),                                                                                                         
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Regular Header', 'dazzle' ),
        'id'         => 'admin-header-regular',
        'subsection' => 'true',
        'fields'     => array(
                    array(
                        'id'        => 'dazzle_menu_type',
                        'type'      => 'select',
                        'title'     => esc_html__('Menu Style', 'dazzle'),
                        'desc'  => esc_html__('Select the menu style of the theme.', 'dazzle'),
                        'options'   => array('classic-menu' => 'Classic',  'minimal-menu' => 'Minimal', 'fullscreen-menu' => 'Fullscreen'),
                        'default'   => 'classic-menu',
                    ),                    

                    array(
                        'id'        => 'dazzle_menu_items_style',
                        'type'      => 'select',
                        'title'     => esc_html__('Menu Items Style', 'dazzle'),
                        'desc'  => esc_html__('Select the menu items style for the theme.', 'dazzle'),
                        'options'   => array('strikethrough' => 'Strikethrough',  'underline' => 'Underline', 'noline' => 'No Line'),
                        'default'   => 'strikethrough',
                    ),   

                    array(
                        'id'        => 'dazzle_header_scheme',
                        'type'      => 'select',
                        'title'     => esc_html__('Header Mood Scheme', 'dazzle'),
                        'desc'  => esc_html__('Select a scheme for the header. Dark or Light. This will mainly affect the navigation. Then pick a color from below.', 'dazzle'),
                        'options'   => array('light-header' => 'Light Background / Dark Navigation', 'dark-header' => 'Dark Background / Light Navigation'),
                        'default'   => 'light-header',
                    ),    

                    array(
                        'id'        => 'dazzle_header_background',
                        'type'      => 'color_rgba',
                        'compiler'  => 'true',
                        // 'output'    => array('background' => '#header'),
                        'title'     => esc_html__('Header Background Color', 'dazzle'),
                        'desc'  => esc_html__('Leave blank if you want to keep the default background color or pick a color for the header (default: white).', 'dazzle'),
                        'default'   => array(
                            'color'     => '#ffffff',
                            'alpha'     => 1
                        ),
                    ),           

                    array(
                        'id'            => 'dazzle_initial_header_padding',
                        'type'          => 'spacing',
                        'mode'          => 'padding',    // absolute, padding, margin, defaults to padding
                        'top'           => true,     // Disable the top
                        'right'         => false,     // Disable the right
                        'bottom'        => true,     // Disable the bottom
                        'left'          => false,     // Disable the left
                        'title'         => esc_html__('Padding-Top/Padding-Bottom values for header`s initial position', 'dazzle'),
                        'desc'      => esc_html__('Set new padding values for the header`s look on initial position.', 'dazzle'),
                        'desc'          => esc_html__('Values are defined in pixels. Default: 48 with 48', 'dazzle'),
                        'default'       => array(
                            'padding-top'    => '40', 
                            'padding-bottom' => '40', 
                        )
                    ), 

                   array(
                        'id'        => 'dazzle_floating_header',
                        'type'      => 'switch',
                        'title'     => esc_html__('Sticky Header', 'dazzle'),
                        'desc'  => esc_html__('You can enable a floating/sticky top-bar header which will include your logo and menu. If disabled, the scrolling effect from below will be ignored.', 'dazzle'),
                        'default'   => 1,
                        'on'        => 'On',
                        'off'       => 'Off'
                    ),                 

                    array(
                        'id'        => 'dazzle_scrolling_effect',
                        'type'      => 'switch',
                        'title'     => esc_html__('Scrolling Effect', 'dazzle'),
                        'desc'  => esc_html__('You can disable the scrolling effect of the header. If disabled, "Padding-Top/Padding-Bottom values for header`s on scroll position" will be ignored.', 'dazzle'),
                        'default'   => 1,
                        'on'        => 'On',
                        'off'       => 'Off'
                    ),   

                    array(
                        'id'        => 'dazzle_slide_sidebar',
                        'type'      => 'switch',
                        'title'     => esc_html__('Vertical Sliding Menu', 'dazzle'),
                        'desc'  => esc_html__('Enable an off-canvas sliding sidebar on the website. A new menu item(...) will be added to the header.', 'dazzle'),
                        'default'   => 0,
                        'on'        => 'On',
                        'off'       => 'Off'
                    ),                       

                    array(
                        'id'            => 'dazzle_onscroll_header_padding',
                        'type'          => 'spacing',
                        'mode'          => 'padding',    // absolute, padding, margin, defaults to padding
                        'top'           => true,     // Disable the top
                        'right'         => false,     // Disable the right
                        'bottom'        => true,     // Disable the bottom
                        'left'          => false,     // Disable the left
                        'required'  => array('dazzle_scrolling_effect', '=', '1'),
                        'title'         => esc_html__('Padding-Top/Padding-Bottom values for header`s on scroll position', 'dazzle'),
                        'desc'      => esc_html__('Set new padding values for the header`s look on scroll position.', 'dazzle'),
                        'desc'          => esc_html__('Values are defined in pixels. Default: 16 with 16', 'dazzle'),
                        'default'       => array(
                            'padding-top'    => '16', 
                            'padding-bottom' => '16', 
                        )
                    ),   

                    array(
                        'id'        => 'dazzle_header_scheme_on_scroll',
                        'type'      => 'select',
                        'title'     => esc_html__('Header Mood Scheme on Scroll', 'dazzle'),
                        'desc'  => esc_html__('Select a scheme for the header. Dark or Light. This will mainly affect the navigation. Then pick a color from below.', 'dazzle'),
                        'options'   => array('light-header' => 'Light Background / Dark Navigation', 'dark-header' => 'Dark Background / Light Navigation'),
                        'default'   => 'light-header',
                    ), 

                    array(
                        'id'        => 'dazzle_header_background_on_scroll',
                        'type'      => 'color_rgba',
                        'title'     => esc_html__('Header Background Color on Scroll', 'dazzle'),
                        'desc'  => esc_html__('Leave blank if you want to keep the default background color or pick a color for the header (default: white - 90% transparent).', 'dazzle'),
                        'default'   => array(
                            'color'     => '#ffffff',
                            'alpha'     => 0.9
                        ),
                    ),   


                    array(
                        'id'        => 'dazzle_search_header',
                        'type'      => 'switch',
                        'title'     => esc_html__('Search Widget in Header', 'dazzle'),
                        'desc'  => esc_html__('You can enable a search icon widget in the header.', 'dazzle'),
                        'default'   => 0,
                        'on'        => 'On',
                        'off'       => 'Off'
                    ),                        

            )
        ));

    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Left Side Header', 'dazzle' ),
        'id'         => 'admin-header-leftside',
        'subsection' => 'true',
        ));    


    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Footer', 'dazzle' ),
        'id'         => 'admin-footer',
        'icon'       => 'el el-icon-minus',
        'fields'     => array(
                    array(
                        'id'        => 'dazzle_copyright_textarea',
                        'type'      => 'editor',
                        'title'     => esc_html__('Footer Text', 'dazzle'),
                        'desc'  => esc_html__('Place here your copyright line. For ex: Copyright 2016 | My website.', 'dazzle'),
                        'default'   => 'COPYRIGHT &copy; DAZZLE STUDIO. ALL RIGHTS RESERVED',
                    ),   
                    array(
                        'id'        => 'dazzle_footer_layout',
                        'type'      => 'button_set',
                        'title'     => esc_html__('Footer Layout', 'dazzle'),
                        'subtitle'  => esc_html__('Set the look of the footer: content on right-left sides, or content centered.', 'dazzle'),
                        
                        //Must provide key => value pairs for radio options
                        'options'   => array(
                            'footer-sides' => 'Content on Sides', 
                            'footer-centered' => 'Content Centered'
                        ), 
                        'default'   => 'footer-centered'
                    ),   
                   array(
                        'id'        => 'dazzle_footer_logo',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => esc_html__('Footer Logo', 'dazzle'),
                        'desc'  => esc_html__('You can add a logo inside the footer(Optional).', 'dazzle'),
                        'default'   => array('url' => 'https://deliciousthemes.com/logo-dazzle.png')                
                    ),  
                   array(
                        'id'        => 'dazzle_svg_footer_enabled',
                        'type'      => 'switch',
                        'title'     => esc_html__('Use SVG Logo', 'dazzle'),
                        'desc'  => esc_html__('You can use an .svg logo instead of a regular .png or .jpg logo.', 'dazzle'),
                        'default'   => 0,
                        'on'        => 'Yes',
                        'off'       => 'No',
                    ),  
                   array(
                        'id'        => 'dazzle_section-svglogo-footer-start',
                        'type'      => 'section',
                        'indent'    => true, // Indent all options below until the next 'section' option is set.
                        'required'  => array('dazzle_svg_footer_enabled', '=', '1'),
                    ),

                    array(
                        'id'        => 'dazzle_svg_footer_logo',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => esc_html__('Upload an SVG Logo', 'dazzle'),
                        //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                        'desc'  => esc_html__('Upload your SVG logo. Make sure to set the width and height in the next fields.', 'dazzle'),
                        'default'   => ''                
                    ),  

                    array(
                        'id'        => 'dazzle_svg_footer_logo_width',
                        'type'      => 'text',
                        'title'     => esc_html__('SVG Logo Width', 'dazzle'),
                        'desc'  => esc_html__('If you enter 100, the logo width will be set to 100px. ', 'dazzle'),
                        'desc'      => esc_html__('Use numbers only', 'dazzle'),
                        'validate'  => 'numeric',
                        'default'   => '85'
                    ),        

                    array(
                        'id'        => 'dazzle_svg_footer_logo_height',
                        'type'      => 'text',
                        'title'     => esc_html__('SVG Logo Height', 'dazzle'),
                        'desc'  => esc_html__('If you enter 50, the logo height will be set to 50px. ', 'dazzle'),
                        'desc'      => esc_html__('Use numbers only', 'dazzle'),
                        'validate'  => 'numeric',
                        'default'   => '25'
                    ),                                            

                    array(
                        'id'        => 'section-svglogo-footer-end',
                        'type'      => 'section',
                        'indent'    => false, // Indent all options below until the next 'section' option is set.
                        'required'  => array('dazzle_svg_footer_enabled', "=", 1),
                    ),                        


        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Blog', 'dazzle' ),
        'icon'       => 'el el-website',
        'id'         => 'admin-blog',
        'fields'     => array(
                   array(
                        'id'        => 'dazzle_blog_sidebar_pos',
                        'type'      => 'image_select',
                        'title'     => esc_html__('Sidebar Position for Blog Related Pages', 'dazzle'),
                        'desc'  => esc_html__('Select a sidebar position for blog related pages. It will be applied to single posts, index page, archive and search pages.', 'dazzle'),
                        'options'   => array(
                            'sidebar-right' => array('alt' => 'Sidebar Right',  'img' => ReduxFramework::$_url . 'assets/img/2cr.png'),
                            'sidebar-left' => array('alt' => 'Sidebar Left',  'img' => ReduxFramework::$_url . 'assets/img/2cl.png'),
                            'no-blog-sidebar' => array('alt' => 'No Sidebar',  'img' => ReduxFramework::$_url . 'assets/img/1col.png')
                        ),
                        'default'   => 'sidebar-right'
                    ),  
                    array(
                        'id'        => 'dazzle_blog_sidebar',
                        'type'      => 'select',
                        'title'     => esc_html__('Sidebar Name for Blog Related Pages', 'dazzle'),
                        'desc'  => esc_html__('Select the sidebar which will be applied to blog related pages, including single posts, index page, archive pages and search result pages.', 'dazzle'),
                        'data'      => 'sidebars',
                        'default' => 'sidebar',
                    ),                      

                    array(
                        'id'        => 'dazzle_tags_list',
                        'type'      => 'switch',
                        'title'     => esc_html__('Enable Tags list on Blog Posts', 'dazzle'),
                        'desc'  => esc_html__('If the option is on, the tags list will be displayed at the bottom of the post.', 'dazzle'),
                        'default'   => 1,
                        'on'        => 'On',
                        'off'       => 'Off',
                    ),                                                      
                    array(
                        'id'        => 'dazzle_social_box',
                        'type'      => 'switch',
                        'title'     => esc_html__('Enable Social Share Icons for Blog Posts Inner Pages', 'dazzle'),
                        'desc'  => esc_html__('If the option is on, the social icons for sharing the post will be displayed.', 'dazzle'),
                        'default'   => 1,
                        'on'        => 'On',
                        'off'       => 'Off',
                    ), 
                    array(
                        'id'        => 'dazzle_author_box',
                        'type'      => 'switch',
                        'title'     => esc_html__('Enable Author Box for Blog Posts Inner Pages', 'dazzle'),
                        'desc'  => esc_html__('If the option is on, the author box will be displayed.', 'dazzle'),
                        'default'   => 1,
                        'on'        => 'On',
                        'off'       => 'Off',
                    ),    
                    array(
                        'id'        => 'dazzle_prev_next_posts',
                        'type'      => 'switch',
                        'title'     => esc_html__('Enable Prev/Next Posts Links for Blog Posts', 'dazzle'),
                        'desc'  => esc_html__('If the option is on, links for Prev/Next posts will be displayed.', 'dazzle'),
                        'default'   => 1,
                        'on'        => 'On',
                        'off'       => 'Off',
                    ),   
                    array(
                        'id'        => 'dazzle_breadcrumbs_blog_url',
                        'type'      => 'text',
                        'title'     => esc_html__('Link URL for the `Blog` link in breadcrumbs.', 'dazzle'),
                        'desc'  => esc_html__('Add an URL for the Blog link in the breadcrumbs which appear on posts.', 'dazzle')
                    ),  
                    array(
                        'id'        => 'dazzle_breadcrumbs_blog_keyword',
                        'type'      => 'text',
                        'default'   => 'The Journal',
                        'title'     => esc_html__('Breadcrumb Keyword for Blog.', 'dazzle'),
                        'desc'  => esc_html__('Ex: Blog or Journal.', 'dazzle')
                    ),                                                                        
        )
    ) ); 

    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Portfolio', 'dazzle' ),
        'id'         => 'admin-portfolio',
        'icon'       => 'el el-icon-briefcase',
        'fields'     => array(
                    array(
                        'id'        => 'dazzle_proj_nav_enabled',
                        'type'      => 'switch',
                        'title'     => esc_html__('Project Navigation', 'dazzle'),
                        'desc'  => esc_html__('Enable/Disable the project navigation from project pages.', 'dazzle'),
                        'default'   => 1,
                        'on'        => 'On',
                        'off'       => 'Off',
                    ),  
                   array(
                        'id'        => 'section-projnav-start',
                        'type'      => 'section',
                        'indent'    => true, // Indent all options below until the next 'section' option is set.
                        'required'  => array('dazzle_proj_nav_enabled', '=', '1'),
                    ),

                    array(
                        'id'        => 'dazzle_portfolio_back_link',
                        'type'      => 'text',
                        'title'     => esc_html__('Link URL for the portfolio `Back` button icon', 'dazzle'),
                        'desc'  => esc_html__('Add an URL for the portfolio Back button.', 'dazzle'),
                        'hint'      => array(
                            //'title'     => '',
                            'content'   => 'Default URL is set to homepage. Ex: http://website.com#work. The URL will be also used to highlight the menu item in the navigation.',
                        )                        
                    ),

                    array(
                        'id'       => 'dazzle_portfolio_nav_behaviour',
                        'type'     => 'radio',
                        'title'    => esc_html__('Project Navigation Behavior:', 'dazzle'), 
                        'desc'     => esc_html__('Select how would you like the navigation to behave: Display link to another project from the same category or not.', 'dazzle'),
                        'options'  => array(
                            'all-categories' => 'Projects from all categories',
                            'same-category' => 'Projects from the same category', 
                        ),
                        'default' => 'all-categories'
                    ),         

                    array(
                        'id'       => 'dazzle_portfolio_nav_reverse_logic',
                        'type'     => 'switch',
                        'title'    => esc_html__('Reverse Navigation Logic:', 'dazzle'), 
                        'desc'     => esc_html__('Set to ON to reverse the logic of the next and prev buttons.', 'dazzle'),
                        'default'   => 0,
                        'on'        => 'On',
                        'off'       => 'Off',
                    ),                               

                    array(
                        'id'        => 'section-projnav-end',
                        'type'      => 'section',
                        'indent'    => false, // Indent all options below until the next 'section' option is set.
                        'required'  => array('dazzle_proj_nav_enabled', '=', '1'),
                    ),     

 
                    array(
                        'id'        => 'dazzle_portfolio_slug',
                        'type'      => 'text',
                        'default'   => 'portfolio',
                        'title'     => esc_html__('Portfolio Slug URL', 'dazzle'),
                        'hint'  => array( 
                            'content' => esc_html__('Change the default portfolio slug URL. Currently, this is set to <strong>portfolio</strong>. Ex: http://website.com/portfolio/project-name. Changing it to <strong>works</strong>, the URLs will become http://website.com/works/project-name. Once you`ll change it, you`ll probably get 404 error pages for projects. To fix this, refresh the permalinks: go to Settings->Permalinks and click on Default. Save. Then click on your custom URL structure(Postname) and save again.', 'dazzle')
                            ),                 
                    ), 

                    array(
                        'id'        => 'dazzle_breadcrumbs_portfolio_url',
                        'type'      => 'text',
                        'title'     => esc_html__('Link URL for the `Portfolio` link in breadcrumbs.', 'dazzle'),
                        'desc'  => esc_html__('Add an URL for the portfolio link in the breadcrumbs which appear on projects.', 'dazzle')
                       
                    ),
                    array(
                        'id'        => 'dazzle_breadcrumbs_portfolio_keyword',
                        'type'      => 'text',
                        'default'   => 'Projects',
                        'title'     => esc_html__('Breadcrumb Keyword for Portfolio.', 'dazzle'),
                        'desc'  => esc_html__('Ex: Projects or Work.', 'dazzle')
                    ),                                                  
        )
    ) );     

    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Typography', 'dazzle' ),
        'id'         => 'admin-typography',
        'icon'       => 'el-icon-filter',
        'fields'     => array(
                   array(
                        'id'        => 'dazzle_typo_info',
                        'type'      => 'info',
                        'title'  => esc_html__('Typography Options', 'dazzle'),
                        'desc'      => esc_html__('The theme is using Google Fonts to render the typography style for your website. You can however, make use of default fonts.).', 'dazzle')
                    ),                    

                    array(
                        'id'        => 'dazzle_body_font_typo',
                        'type'      => 'typography',
                        'output'    => array('html body'),
                        'title'     => esc_html__('Body Font Options', 'dazzle'),
                        'desc'  => esc_html__('Select font options for the body', 'dazzle'),
                        'google'    => true,
                        'text-align'=> false,
                        'all_styles'=> true,
                        'subsets'   => true,
                        'default'   => array(
                            'google'      => true,
                            'color'         => '#656565',
                            'font-size'     => '18px',
                            'font-family'   => 'Lato',
                            'line-height'   => '30px',
                            'font-weight'   => '300',
                        ),
                    ),

                    array(
                        'id'        => 'dazzle_menu_typo',
                        'type'      => 'typography',
                        'output'    => array('html .main-navigation li a', '#dazzle-left-side .menu li a'),
                        'title'     => esc_html__('Menu Font Options', 'dazzle'),
                        'desc'  => esc_html__('Select font options for the main menu.', 'dazzle'),
                        'google'    => true,
                        'text-align'=> false,
                        'subsets'   => true,
                        'default'   => array(
                            'google'      => true,
                            'color'         => '#323232',
                            'font-size'     => '13px',
                            'font-family'   => 'Lato',
                            'line-height'   => '22px',
                            'font-weight'   => '400',
                        ),
                    ),

                    array(
                        'id'        => 'dazzle_submenu_typo',
                        'type'      => 'typography',
                        'output'    => array('html .main-navigation ul ul a'),
                        'title'     => esc_html__('Dropdown Menu Font Options', 'dazzle'),
                        'desc'  => esc_html__('Select font options for the submenu/dropdown items.', 'dazzle'),
                        'text-align'=> false,
                        'subsets'   => true,
                        'color' => false,
                        'default'   => array(
                            'google'      => true,
                            'font-size'     => '15px',
                            'font-family'   => 'Lato',
                            'line-height'   => '24px',
                            'font-weight'   => '400'
                        ),
                    ),                    

                    array(
                        'id'        => 'dazzle_h1_typo',
                        'type'      => 'typography',
                        'output'    => array('html h1'),
                        'title'     => esc_html__('H1 Font Options', 'dazzle'),
                        'desc'  => esc_html__('Select font options for Heading 1.', 'dazzle'),
                        'google'    => true,
                        'text-align'=> false,
                        'subsets'   => true,
                        'default'   => array(
                            'google'      => true,
                            'color'         => '#323232',
                            'font-size'     => '42px',
                            'font-family'   => 'Montserrat',
                            'line-height'   => '52px',
                            'font-weight'   => '400',
                        ),
                    ),  

                    array(
                        'id'        => 'dazzle_h2_typo',
                        'type'      => 'typography',
                        'output'    => array('html h2'),
                        'title'     => esc_html__('H2 Font Options', 'dazzle'),
                        'desc'  => esc_html__('Select font options for Heading 2.', 'dazzle'),
                        'google'    => true,
                        'text-align'=> false,
                        'subsets'   => true,
                        'default'   => array(
                            'google'      => true,
                            'color'         => '#323232',
                            'font-size'     => '30px',
                            'font-family'   => 'Montserrat',
                            'line-height'   => '42px',
                            'font-weight'   => '400',
                        ),
                    ),  

                    array(
                        'id'        => 'dazzle_h3_typo',
                        'type'      => 'typography',
                        'output'    => array('html h3'),
                        'title'     => esc_html__('H3 Font Options', 'dazzle'),
                        'desc'  => esc_html__('Select font options for Heading 3.', 'dazzle'),
                        'google'    => true,
                        'text-align'=> false,
                        'subsets'   => true,
                        'default'   => array(
                            'google'      => true,
                            'color'         => '#323232',
                            'font-size'     => '24px',
                            'font-family'   => 'Montserrat',
                            'line-height'   => '32px',
                            'font-weight'   => '400',
                        ),
                    ),  

                    array(
                        'id'        => 'dazzle_h4_typo',
                        'type'      => 'typography',
                        'output'    => array('html h4'),
                        'title'     => esc_html__('H4 Font Options', 'dazzle'),
                        'desc'  => esc_html__('Select font options for Heading 4.', 'dazzle'),
                        'google'    => true,
                        'text-align'=> false,
                        'subsets'   => true,
                        'default'   => array(
                            'google'      => true,
                            'color'         => '#323232',
                            'font-size'     => '18px',
                            'font-family'   => 'Montserrat',
                            'line-height'   => '28px',
                            'font-weight'   => '400',
                        ),
                    ),      

                    array(
                        'id'        => 'dazzle_h5_typo',
                        'type'      => 'typography',
                        'output'    => array('html h5'),
                        'title'     => esc_html__('H5 Font Options', 'dazzle'),
                        'desc'  => esc_html__('Select font options for Heading 5.', 'dazzle'),
                        'google'    => true,
                        'text-align'=> false,
                        'subsets'   => true,
                        'default'   => array(
                            'google'      => true,
                            'color'         => '#323232',
                            'font-size'     => '15px',
                            'font-family'   => 'Montserrat',
                            'line-height'   => '24px',
                            'font-weight'   => '400',
                        ),
                    ),       

                    array(
                        'id'        => 'dazzle_h6_typo',
                        'type'      => 'typography',
                        'output'    => array('html h6'),
                        'title'     => esc_html__('H6 Font Options', 'dazzle'),
                        'desc'  => esc_html__('Select font options for Heading 6.', 'dazzle'),
                        'google'    => true,
                        'text-align'=> false,
                        'subsets'   => true,
                        'default'   => array(
                            'google'      => true,
                            'color'         => '#323232',
                            'font-size'     => '14px',
                            'font-family'   => 'Montserrat',
                            'line-height'   => '20px',
                            'font-weight'   => '700',
                        ),
                    ),  
                    array(
                        'id'        => 'dazzle_secondary_typo',
                        'type'      => 'typography',
                        'title'     => esc_html__('Secondary Font Options', 'dazzle'),
                        'desc'  => esc_html__('Select the secondary font options. Only some elements will be affected.', 'dazzle'),
                        'google'    => true,
                        'text-align'=> false,
                        'font-size'=> false,
                        'color'=> false,
                        'line-height'=> false,
                        'subsets'   => true,
                        'default'   => array(
                            'google'      => true,
                            'font-family'   => 'Montserrat',
                            'font-weight'   => '400'
                        ),
                    ),                        
                                                     
        )
    ) );    

    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Styling', 'dazzle' ),
        'id'         => 'admin-styling',
        'icon'       => 'el-icon-brush',
        'fields'     => array(
                    array(
                        'id'        => 'dazzle_custom_color_scheme',
                        'type'      => 'color',
                        'title'     => esc_html__('Color Scheme', 'dazzle'),
                        'desc'  => esc_html__('Set the color for the scheme.', 'dazzle'),
                        'default'   => '#f39c12',
                        'transparent' => false,
                        'validate'  => 'color',
                    ),
                    array(
                        'id'        => 'dazzle_body_background',
                        'type'      => 'color',
                        'output'    => array('background-color' => 'html body'),
                        'title'     => esc_html__('Body Background Color', 'dazzle'),
                        'desc'  => esc_html__('Leave blank or pick a color for the body. (default: #fafafa).', 'dazzle'),
                        'default'   => '#fafafa',
                        'transparent' => false,
                        'validate'  => 'color',
                    ),
                    array(
                        'id'        => 'dazzle_wrapper_background',
                        'type'      => 'color',
                        'output'    => array('background-color' => 'html body #page'),
                        'title'     => esc_html__('Content Wrapper Background Color', 'dazzle'),
                        'desc'  => esc_html__('Leave blank if you want to keep the default background color or pick a color for the content wrapper (default: #fff).', 'dazzle'),
                        'default'   => '#ffffff',
                        'transparent' => false,
                        'validate'  => 'color'
                    ),           
                    array(
                        'id'        => 'dazzle_footer_background',
                        'type'      => 'color',
                        'output'    => array('background-color' => 'html .site-footer'),
                        'title'     => esc_html__('Footer Background Color', 'dazzle'),
                        'desc'  => esc_html__('Leave blank if you want to keep the default background color or pick a color for the footer (default: #fafafa).', 'dazzle'),
                        'default'   => '#fafafa',
                        'transparent' => false,
                        'validate'  => 'color'
                    ),   
                    array(
                        'id'        => 'dazzle_selected_text_background',
                        'type'      => 'color',
                        'output'    => array('background' => '-moz::selection,::selection'),
                        'title'     => esc_html__('Selected Text Background Color', 'dazzle'),
                        'desc'  => esc_html__('Leave blank if you want to keep the default background color or pick a color for the selected text (default: blue, set by the browser).', 'dazzle'),
                        'default'   => '#323232',                      
                        'transparent' => false,
                        'validate'  => 'color'
                    ),                                                   

                    array(
                        'id'        => 'dazzle_pattern',
                        'type'      => 'image_select',
                        'title'     => esc_html__('Patterns for Background', 'dazzle'),
                        'desc'  => esc_html__('Select a pattern and set it as background. Choose between these patterns. More to come...', 'dazzle'),
                        'options'   => array(
                            'bg12' => array('alt' => '',  'img' => get_template_directory_uri() .'/framework/admin/assets/img/bg/bg12.png'),
                            'bg1' => array('alt' => '',  'img' => get_template_directory_uri() .'/framework/admin/assets/img/bg/bg1.png'),
                            'bg2' => array('alt' => '',  'img' => get_template_directory_uri() .'/framework/admin/assets/img/bg/bg2.png'),
                            'bg3' => array('alt' => '',  'img' => get_template_directory_uri() .'/framework/admin/assets/img/bg/bg3.png'),
                            'bg4' => array('alt' => '',  'img' => get_template_directory_uri() .'/framework/admin/assets/img/bg/bg4.png'),
                            'bg5' => array('alt' => '',  'img' => get_template_directory_uri() .'/framework/admin/assets/img/bg/bg5.png'),
                            'bg6' => array('alt' => '',  'img' => get_template_directory_uri() .'/framework/admin/assets/img/bg/bg6.png'),
                            'bg7' => array('alt' => '',  'img' => get_template_directory_uri() .'/framework/admin/assets/img/bg/bg7.png'),
                            'bg8' => array('alt' => '',  'img' => get_template_directory_uri() .'/framework/admin/assets/img/bg/bg8.png'),
                            'bg9' => array('alt' => '',  'img' => get_template_directory_uri() .'/framework/admin/assets/img/bg/bg9.png'),
                            'bg10' => array('alt' => '',  'img' => get_template_directory_uri() .'/framework/admin/assets/img/bg/bg10.png'),
                            'bg11' => array('alt' => '',  'img' => get_template_directory_uri() .'/framework/admin/assets/img/bg/bg11.png'),
                            'bg14' => array('alt' => '',  'img' => get_template_directory_uri() .'/framework/admin/assets/img/bg/bg14.png'),
                            'bg15' => array('alt' => '',  'img' => get_template_directory_uri() .'/framework/admin/assets/img/bg/bg15.png'),
                            'bg16' => array('alt' => '',  'img' => get_template_directory_uri() .'/framework/admin/assets/img/bg/bg16.png'),
                            'bg17' => array('alt' => '',  'img' => get_template_directory_uri() .'/framework/admin/assets/img/bg/bg17.png'),
                            'bg18' => array('alt' => '',  'img' => get_template_directory_uri() .'/framework/admin/assets/img/bg/bg18.png'),
                            'bg19' => array('alt' => '',  'img' => get_template_directory_uri() .'/framework/admin/assets/img/bg/bg19.png')
                        ),
                        'default'   => 'bg12'
                    ),                             
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Social', 'dazzle' ),
        'id'         => 'admin-social',
        'icon'       => 'el-icon-network',
        'fields'     => array(
                   array(
                        'id'        => 'dazzle_social_intro',
                        'type'      => 'info',
                        'title'  => esc_html__('Social Options.', 'dazzle'),
                        'desc'      => esc_html__('Set your social network references. Add your links for popular platforms like Twitter and Facebook. If you don`t want to include a social icon in the list, just leave the textfield empty.).', 'dazzle')
                    ),
                    array(
                        'id'        => 'rss',
                        'type'      => 'text',
                        'title'     => esc_html__('Your RSS Feed address', 'dazzle'),
                        'default'   => 'http://feeds.feedburner.com/EnvatoNotes'
                    ),   
                    array(
                        'id'        => 'facebook',
                        'type'      => 'text',
                        'title'     => esc_html__('Your Facebook page/profile URL', 'dazzle'),
                        'default'   => 'http://www.facebook.com/envato'
                    ),  
                    array(
                        'id'        => 'twitter',
                        'type'      => 'text',
                        'title'     => esc_html__('Your Twitter URL', 'dazzle'),
                        'default'   => 'http://twitter.com/envato'
                    ),  
                    array(
                        'id'        => 'flickr',
                        'type'      => 'text',
                        'title'     => esc_html__('Your Flickr Page URL', 'dazzle'),
                    ),    
                    array(
                        'id'        => 'google-plus',
                        'type'      => 'text',
                        'title'     => esc_html__('Your Google Plus Page URL', 'dazzle'),
                    ),  
                    array(
                        'id'        => 'dribbble',
                        'type'      => 'text',
                        'title'     => esc_html__('Your Dribbble Profile URL', 'dazzle'),
                    ), 
                    array(
                        'id'        => 'pinterest',
                        'type'      => 'text',
                        'title'     => esc_html__('Your Pinterest URL', 'dazzle'),
                    ), 
                    array(
                        'id'        => 'linkedin',
                        'type'      => 'text',
                        'title'     => esc_html__('Your LinkedIn Profile URL', 'dazzle'),
                    ), 
                    array(
                        'id'        => 'skype',
                        'type'      => 'text',
                        'title'     => esc_html__('Your Skype Username', 'dazzle'),
                    ), 
                    array(
                        'id'        => 'github-alt',
                        'type'      => 'text',
                        'title'     => esc_html__('Your Github URL', 'dazzle'),
                    ), 
                    array(
                        'id'        => 'youtube',
                        'type'      => 'text',
                        'title'     => esc_html__('Your YouTube URL', 'dazzle'),
                    ), 
                    array(
                        'id'        => 'vimeo-square',
                        'type'      => 'text',
                        'title'     => esc_html__('Your Vimeo Page URL', 'dazzle'),
                    ), 
                    array(
                        'id'        => 'instagram',
                        'type'      => 'text',
                        'title'     => esc_html__('Your Instagram Profile URL', 'dazzle'),
                    ),

                    array(
                        'id'        => 'tumblr',
                        'type'      => 'text',
                        'title'     => esc_html__('Your Tumblr URL', 'dazzle'),
                    ),   

                    array(
                        'id'        => 'behance',
                        'type'      => 'text',
                        'title'     => esc_html__('Your Behance Profile URL', 'dazzle'),
                    ),                      

                    array(
                        'id'        => 'vk',
                        'type'      => 'text',
                        'title'     => esc_html__('Your VK URL', 'dazzle'),
                    ), 

                    array(
                        'id'        => 'xing',
                        'type'      => 'text',
                        'title'     => esc_html__('Your Xing URL', 'dazzle'),
                    ),   
                    array(
                        'id'        => 'soundcloud',
                        'type'      => 'text',
                        'title'     => esc_html__('Your SoundCloud URL', 'dazzle'),
                    ),    
                    array(
                        'id'        => 'codepen',
                        'type'      => 'text',
                        'title'     => esc_html__('Your Codepen URL', 'dazzle'),
                    ),                                                                                              
                    array(
                        'id'        => 'yelp',
                        'type'      => 'text',
                        'title'     => esc_html__('Your Yelp URL', 'dazzle'),
                    ),   
                    array(
                        'id'        => 'slideshare',
                        'type'      => 'text',
                        'title'     => esc_html__('Your Slideshare URL', 'dazzle'),
                    ),        
                    array(
                        'id'        => 'houzz',
                        'type'      => 'text',
                        'title'     => esc_html__('Your Houzz URL', 'dazzle'),
                    ),   
                    array(
                        'id'        => '500px',
                        'type'      => 'text',
                        'title'     => esc_html__('Your 500px URL', 'dazzle'),
                    ),                                   

                    array(
                        'id'        => 'dazzle_header_social',
                        'type'      => 'switch',
                        'title'     => esc_html__('Social Icons in Header', 'dazzle'),
                        'desc'  => esc_html__('Enable/Disable social icons for the header. If enabled, the social icons block will be displayed in the header nav bar.', 'dazzle'),
                        'default'   => 0,
                        'on'        => 'On',
                        'off'       => 'Off'
                    ),                     
        )
    ) );


if ( class_exists( 'Woocommerce' ) ) {  
   Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'WooCommerce', 'dazzle' ),
        'id'         => 'admin-woocommerce',
        'icon'       => 'el-icon-shopping-cart',
        'fields'     => array(
                      array(
                            'id'        => 'dazzle_woo_layout',
                            'type'      => 'image_select',
                            'title'     => __('Sidebar Position for the Shop Page', 'dazzle'),
                            'subtitle'  => __('Select a sidebar position for the Shop page.', 'dazzle'),
                            'options'   => array(
                                'sidebar-right' => array('alt' => 'Sidebar Right',  'img' => ReduxFramework::$_url . 'assets/img/2cr.png'),
                                'sidebar-left' => array('alt' => 'Sidebar Left',  'img' => ReduxFramework::$_url . 'assets/img/2cl.png'),
                                'no-sidebar' => array('alt' => 'No Sidebar',  'img' => ReduxFramework::$_url . 'assets/img/1col.png')
                            ),
                            'default'   => 'sidebar-right'
                        ),  
                        array(
                            'id'        => 'dazzle_woo_sidebar',
                            'type'      => 'select',
                            'title'     => __('Sidebar Name for Shop Page', 'dazzle'),
                            'subtitle'  => __('Select the sidebar which will be applied to the shop page, if the shop page layout defined from the option from above is set to a sidebar.', 'dazzle'),
                            'data'      => 'sidebars',
                            'default' => 'sidebar',
                        ),
                    array(
                        'id'        => 'dazzle_woo_products_per_row',
                        'type'      => 'select',
                        'title'     => __('Products per Row', 'dazzle'),
                        'subtitle'  => __('Set how many products would you like to display on a single row. In other words, how many columns will the shop page have?', 'dazzle'),
                        'options'   => array('2' => '2',  '3' => '3', '4' => '4', '6' => '6'),
                        'default'   => '3',
                    ),   
                    array(
                        'id'        => 'dazzle_woo_products_per_page',
                        'type'      => 'slider',
                        'title'     => __('Products per Page', 'dazzle'),
                        'subtitle'  => __('Set how many products would you like to display on a page.', 'dazzle'),
                        'desc'      => esc_html__('Use numbers only', 'dazzle'),
                        'default'       => 9,
                        'min'           => 1,
                        'step'          => 1,
                        'max'           => 50,
                        'display_value' => 'text'                           
                    ),                                         
                ),
        )
   );
}


    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Custom CSS & JS', 'dazzle' ),
        'id'         => 'admin-custom-code',
        'icon'       => 'el-icon-edit',
        'fields'     => array(
                    array(
                        'id'        => 'dazzle_more_css',
                        'type'      => 'ace_editor',
                        'mode'      => 'css',
                        'theme'     => 'chrome',
                        'title'     => esc_html__('Custom CSS', 'dazzle'),
                        'desc'  => esc_html__('Quickly add some CSS to your theme by adding it to this block.', 'dazzle'),
                        'validate'  => 'css',
                        'options'   => array('minLines' => 12, 'useWorker' => false, 'fontSize' => 13)
                    ),

                    array(
                        'id'        => 'dazzle__header_custom_js',
                        'type'      => 'ace_editor',
                        'title'     => esc_html__('Header Custom JS', 'dazzle'),
                        'desc'  => esc_html__('Paste your JavaScript code here. Use this field to quickly add JS code snippets before </head>.', 'dazzle'),
                        'mode'      => 'javascript',
                        'theme'     => 'chrome',
                        'options'   => array('minLines' => 12, 'fontSize' => 13),
                        'default'   => ""
                    ),                      

                    array(
                        'id'        => 'dazzle__footer_custom_js',
                        'type'      => 'ace_editor',
                        'title'     => esc_html__('Footer Custom JS', 'dazzle'),
                        'desc'  => esc_html__('Paste your JavaScript code here. Use this field to quickly add JS code snippets.', 'dazzle'),
                        'mode'      => 'javascript',
                        'theme'     => 'chrome',
                        'options'   => array('minLines' => 12, 'fontSize' => 13),
                        'default'   => ""
                    ),                    

        )
    ) );

    /*
     * <--- END SECTIONS
     */