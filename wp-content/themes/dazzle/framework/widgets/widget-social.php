<?php
/******************************************
/* Social Widget
******************************************/
class dazzle_Social_Widget extends WP_Widget {

    private $dazzle_networks = array('facebook', 'twitter', 'google', 'instagram', 'pinterest', 'bloglovin', 'linkedin', 'dribbble', 'youtube', 'vimeo', 'flickr', 'github', 'tumblr', 'behance', 'soundcloud', 'email', 'rss');  
              
    /** constructor */
    public function __construct() {
        parent::__construct(
          'dazzle_social_widget', 
          esc_html__('Dazzle - Social', 'dazzle'),
          array (
            'description' => esc_html__( 'Social block widget', 'dazzle' )
          )
          );
    }

    /** @see WP_Widget::widget */
    public function widget($args, $instance) {   
        extract( $args );
        $dazzle_title = apply_filters('widget_title', $instance['title'] );

        foreach($this->dazzle_networks as $dazzle_network) {
          $$dazzle_network = $instance[$dazzle_network];
        }

        echo $args['before_widget'];

        if ( $dazzle_title ) { 
          echo $args['before_title'] . esc_attr($dazzle_title) . $args['after_title'];     
        }
          ?>                
            <ul id="dt-social-widget">
            <?php

            $dazzle_ext = '';
            if('on' == $instance['dazzle_newtab'] ) {
              $dazzle_ext = 'target="_blank"';
            }
            foreach($this->dazzle_networks as $dazzle_network) {
              if($$dazzle_network != '') { 

                if($dazzle_network == 'bloglovin') { 
                  echo '<li class="dt-social-'.$dazzle_network.'"><a href="'.$$dazzle_network.'" '.$dazzle_ext.'><i class="fa fa-heart"></i></a></li>';
                }
                else if($dazzle_network == 'email') { 
                  echo '<li class="dt-social-'.$dazzle_network.'"><a href="mailto:'.$$dazzle_network.'" '.$dazzle_ext.'><i class="fa fa-envelope-o"></i></a></li>';
                }
                else {
                  echo '<li class="dt-social-'.$dazzle_network.'"><a href="'.$$dazzle_network.'" '.$dazzle_ext.'><i class="fa fa-'.$dazzle_network.'"></i></a></li>';                
                }
              }
            }      
        ?>        
            </ul><!--end social-widget-->
                
          <?php 
          echo $args['after_widget'];
    }

    /** @see WP_Widget::update */
    public function update($new_instance, $old_instance) {          
    
      $instance = $old_instance;

      $instance['title'] = strip_tags($new_instance['title']);

      foreach($this->dazzle_networks as $dazzle_network) {
        $instance[$dazzle_network] = strip_tags($new_instance[$dazzle_network]);
      }   

      $instance['dazzle_newtab'] = $new_instance['dazzle_newtab'];
      
      return $instance;
    }

    /** @see WP_Widget::form */
    public function form($instance) {

        $defaults = array();
        foreach($this->dazzle_networks as $dazzle_network) {
          $defaults[$dazzle_network] = '';
        } 
        $defaults['title'] = esc_html__('Connect with Us', 'dazzle');
        $defaults['dazzle_newtab'] = 'on';
        $instance = wp_parse_args( (array) $instance, $defaults );  

        $dazzle_title = $instance['title'];
        ?>

        <p>
          <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Widget Title: ', 'dazzle'); ?></label> 
          <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($dazzle_title); ?>" />
        </p>

        <?php
        foreach($this->dazzle_networks as $dazzle_network) {
          $$dazzle_network = $instance[$dazzle_network]; 
          ?>
         <p>
          <label for="<?php echo esc_attr($this->get_field_id($dazzle_network)); ?>"><?php echo esc_html(ucfirst($dazzle_network)); ?></label> 
          <input class="widefat" id="<?php echo esc_attr($this->get_field_id($dazzle_network)); ?>" name="<?php echo esc_attr($this->get_field_name($dazzle_network)); ?>" type="text" value="<?php echo esc_attr($$dazzle_network); ?>" />
        </p>
          <?php
        }  
        ?>
        <p>
          <input class="checkbox" type="checkbox" <?php checked($instance['dazzle_newtab'], 'on'); ?> id="<?php echo $this->get_field_id('dazzle_newtab'); ?>" name="<?php echo $this->get_field_name('dazzle_newtab'); ?>" /> 
          <label for="<?php echo $this->get_field_id('dazzle_newtab'); ?>">Open links in a new tab</label>
        </p>   
    <?php     
    }

} // class dazzle_Social_Widget
// register Social widget
add_action( 'widgets_init', function(){
     register_widget( 'dazzle_Social_Widget' );
});
?>