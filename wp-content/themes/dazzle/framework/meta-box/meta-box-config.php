<?php

add_filter( 'rwmb_meta_boxes', 'dazzle_register_meta_boxes' );

function dazzle_register_meta_boxes( $dazzle_meta_boxes )
{

	$dazzle_prefix = 'dazzle_';

	$dazzle_img_dir_path = get_template_directory_uri(). '/framework/meta-box/meta-box-extensions/images/';

	$dazzle_class_var = new dazzle_Delicious;
	$dazzle_all_sidebars = $dazzle_class_var->dazzle_my_sidebars();


	$dazzle_menus = wp_get_nav_menus();
	
	global $dazzle_menu_array;
	$dazzle_menu_array = array('' => '-');
	foreach ($dazzle_menus as $dazzle_menu) {
		$dazzle_option = $dazzle_menu->name;
		$dazzle_menu_array[$dazzle_menu->name] = $dazzle_option;
	}	

	$dazzle_meta_boxes[] = array(
		'id'         => 'dazzle_page_layout_metaboxes',
		'title'      => esc_html__( 'Page Layout', 'dazzle' ),
		'post_types' => array( 'page' ),
		'context'    => 'side',
		'priority'   => 'high',
		'autosave'   => true,
		'show'   => array(
			'relation'    => 'OR',
			'template'    => array( 'default', 'template-blog.php'),
		),			
		'fields'     => array(
			array(
				'name'    => esc_html__( 'Sidebar Position', 'dazzle' ),
				'id'      => "{$dazzle_prefix}sidebar_position",
				'type'    => 'image_select',
				'options' => array(
					'no-sidebar' => $dazzle_img_dir_path.'no-blog-sidebar.png',
					'sidebar-right' => $dazzle_img_dir_path.'sidebar-right.png',
					'sidebar-left' => $dazzle_img_dir_path.'sidebar-left.png',
				),
				'std'	=> 'no-sidebar',		
	            'multiple' => false,				
			),
			array(
				'name'        => esc_html__( 'Pick a Sidebar', 'dazzle' ),
				'id'          => "{$dazzle_prefix}all_sidebars",
				'type'        => 'select_advanced',
				'options'     => $dazzle_all_sidebars,
				'multiple'    => false,
			),				
		),
	);			

	$dazzle_meta_boxes[] = array(
		'id'         => 'dazzle_blog_metaboxes',
		'title'      => esc_html__( 'Blog Layout Options', 'dazzle' ),
		'post_types' => array( 'page' ),
		'context'    => 'normal',
		'priority'   => 'high',
		'autosave'   => true,
		'show'   => array(
			'relation'    => 'OR',
			'template'    => array( 'template-blog.php' ),
		),			
		'fields'     => array(
			array(
				'name'    => esc_html__( 'Blog Layout', 'dazzle' ),
				'id'      => "{$dazzle_prefix}blog_layout",
				'type'    => 'image_select',
				'options' => array(
					'masonry-3-cols' => $dazzle_img_dir_path.'masonry-3-cols.png',
					'masonry-2-cols-sr' => $dazzle_img_dir_path.'masonry-2-cols-sidebar-right.png',
					'masonry-2-cols-sl' => $dazzle_img_dir_path.'masonry-2-cols-sidebar-left.png',
					'masonry-2-cols' => $dazzle_img_dir_path.'masonry-2-cols.png',
					'regular-right' => $dazzle_img_dir_path.'sidebar-right.png',
					'regular-left' => $dazzle_img_dir_path.'sidebar-left.png',
					'regular' => $dazzle_img_dir_path.'no-blog-sidebar.png',
				),
				'std'	=> 'masonry',			
			),		
		),
	);		


	$dazzle_meta_boxes[] = array(
		'id'         => 'dazzle_blog_video_metaboxes',
		'title'      => esc_html__( 'Video Post Format Options', 'dazzle' ),
		'post_types' => array( 'post' ),
		'context'    => 'normal',
		'priority'   => 'high',
		'autosave'   => true,
		'visible'   => array( 'post_format', 'video' ),		
		'fields'     => array(
			array(
				'name'  => esc_html__( 'External URL(embed YouTube or Vimeo videos )', 'dazzle' ),
				'id'    => "{$dazzle_prefix}external_video_block",
				'desc'  => esc_html__( 'Use an YouTube or Vimeo page URL(ex: http://www.youtube.com/watch?v=x6qe_kVaBpg). The embed code will be automatically created.', 'dazzle' ),
				'type'  => 'text',
				'size'	=> 50,
			),	
		),
	);	

	$dazzle_meta_boxes[] = array(
		'id'         => 'dazzle_quote_post_metaboxes',
		'title'      => esc_html__( 'Quote Post Format Options', 'dazzle' ),
		'post_types' => array( 'post' ),
		'context'    => 'normal',
		'priority'   => 'high',
		'autosave'   => true,
		'visible'   => array( 'post_format', 'quote' ),		
		'fields'     => array(
			array(
				'name'  => esc_html__( 'Quote Text', 'dazzle' ),
				'id'    => "{$dazzle_prefix}quote_text",
				'type'  => 'textarea',
				'size'	=> 50,
			),	
			array(
				'name'  => esc_html__( 'Quote Author', 'dazzle' ),
				'id'    => "{$dazzle_prefix}quote_author",
				'type'  => 'text',
				'size'	=> 50,
			),				
		),
	);		

	$dazzle_meta_boxes[] = array(
		'id'         => 'dazzle_blog_gallery_metaboxes',
		'title'      => esc_html__( 'Gallery Post Format Options', 'dazzle' ),
		'post_types' => array( 'post' ),
		'context'    => 'normal',
		'priority'   => 'high',
		'autosave'   => true,
		'visible'   => array( 'post_format', 'gallery' ),		
		'fields'     => array(
			array(
				'name'             => esc_html__( 'Post Slider Images', 'dazzle' ),
				'id'               => "{$dazzle_prefix}blog_gallery",
				'type'             => 'image_advanced',
				'max_status'	   => false,
				'max_file_uploads' => 30,
				'desc'			   => esc_html__( 'Upload new images or select them from the media library. (Ctrl/CMD + Click for selecting multiple items at once)', 'dazzle' )
			),	
		),
	);				

if ( post_type_exists( 'portfolio' ) ) {
	$dazzle_meta_boxes[] = array(
		'id'         => 'dazzle_portfolio_metaboxes',
		'title'      => esc_html__( 'Portfolio Options', 'dazzle' ),
		'post_types' => array( 'page' ),
		'context'    => 'normal',
		'priority'   => 'high',
		'autosave'   => true,
		'show'   => array(
			'relation'    => 'OR',
			'template'    => array( 'template-portfolio.php' ),
		),			
		'fields'     => array(
			array(
				'name'    => esc_html__( 'Layout', 'dazzle' ),
				'id'      => "{$dazzle_prefix}portfolio_columns",
				'type'    => 'image_select',
				'options' => array(
					'grid' => $dazzle_img_dir_path.'masonry-3-cols.png'
				),
				'std'	=> 'grid',			
			),		
			array(
				'name'        => esc_html__( 'Grid Columns', 'dazzle' ),
				'id'          => "{$dazzle_prefix}masonry_grid_columns",
				'type'        => 'select_advanced',
				'options'     => array(
						'two-cols' => esc_html__( '2', 'dazzle' ),
						'three-cols' => esc_html__( '3', 'dazzle' ),
						'four-cols' => esc_html__( '4', 'dazzle' ),
						'five-cols' => esc_html__( '5', 'dazzle' ),
					),
				'std' 		  => 'three-cols',
				'multiple'    => false,
			),				
			array(
				'name' => esc_html__( 'With Filter', 'dazzle' ),
				'id'   => "{$dazzle_prefix}portfolio_navigation",
				'desc'  => esc_html__( 'Check the box to enable filters above the portfolio grid.', 'dazzle' ),
				'type' => 'checkbox',
				'std'  => 1,
			),
			array(
				'name'    => esc_html__( 'Categories/Filters', 'dazzle' ),
				'id'      => "{$dazzle_prefix}cats_field",
				'type'    => 'taxonomy',
				'desc'	  => esc_html__('Select from which categories to display projects in the grid.', 'dazzle'),
				'options' => array(
					// Taxonomy name
					'taxonomy' => 'portfolio_cats',
					'type'     => 'checkbox_tree',
					'args'     => array()
				),
			),						
		),
	);	
}


	$dazzle_meta_boxes[] = array(
		'id'         => 'dazzle_standard',
		'title'      => esc_html__( 'Page Options (optional)', 'dazzle' ),
		'post_types' => array( 'page', 'portfolio' ),
		'context'    => 'normal',
		'priority'   => 'high',
		'autosave'   => true,
		'fields'     => array(
			array(
				'name'        => esc_html__( 'Title Position', 'dazzle' ),
				'id'          => "{$dazzle_prefix}page_title_position",
				'type'        => 'select_advanced',
				'options'     => array(
					'left-title'	=> esc_html__( 'Left', 'dazzle' ),
					'center-title'	=> esc_html__( 'Center', 'dazzle' ),					
					),
				'multiple'    => false,
			),				
			array(
				'name'  => esc_html__( 'Title Tagline', 'dazzle' ),
				'id'    => "{$dazzle_prefix}page_tagline",
				'desc'  => esc_html__( 'You can set a tagline for the title.', 'dazzle' ),
				'type'  => 'text',
				'size'	=> 50,
			),
			array(
				'id'               => "{$dazzle_prefix}page_title_bg",
				'name'             => esc_html__( 'Title Background Image', 'dazzle' ),
				'type'             => 'image_advanced',
				'force_delete'     => false,
				'max_status'	   => false,
				// Maximum image uploads
				'max_file_uploads' => 1,
			),			
			array(
				'name' => esc_html__( 'Disable Title Section', 'dazzle' ),
				'id'   => "{$dazzle_prefix}page_title",
				'desc'  => esc_html__( 'Disable the entire page title section and use Visual Composer to build your page on a blank canvas.', 'dazzle' ),
				'type' => 'checkbox',
				'std'  => 0,
			),
			array(
				'name' => esc_html__( 'Disable Solid Header', 'dazzle' ),
				'id'   => "{$dazzle_prefix}solid_header_switch",
				'desc'  => esc_html__( 'Disable the solid header and set it as an overlay for the content. This is known as a fixed header or absolute positioned header.', 'dazzle' ),
				'type' => 'checkbox',
				'std'  => 0,
			),	
			array(
				'name' => esc_html__( 'Push Header at Top', 'dazzle' ),
				'id'   => "{$dazzle_prefix}push_header_top",
				'desc'  => esc_html__( 'Disabling the solid header will position it over the title. Push the header back to top by selecting the checkbox. Best to use when setting a background image for the page title. ', 'dazzle' ),
				'hidden' 	  => array('dazzle_solid_header_switch', '!=', '1'),
				'type' => 'checkbox',
				'std'  => 0,
			),				

			array(
				'type' => 'divider',
				'id'   => 'page_divider', // Not used, but needed
			),

			array(
				'name' => esc_html__( 'Customize the Header Behavior', 'dazzle' ),
				'id'   => "{$dazzle_prefix}pagenav_behavior_switch",
				'desc'  => esc_html__( 'Set a new behavior for the header, like setting new background colors for it or other logo.', 'dazzle' ),
				'type' => 'checkbox',
				'std'  => 0,
			),			

			array(
				'name'        => esc_html__( 'Menu Style', 'dazzle' ),
				'id'          => "{$dazzle_prefix}page_menu_style",
				'type'        => 'select_advanced',
				'hidden' 	  => array('dazzle_pagenav_behavior_switch', '!=', '1'),
				'desc'		  => esc_html__('Overwrite the global menu style, set in Appearance->Delicious Options->Header.','dazzle'),
				'options'     => array(
					'classic-menu' => esc_html__('Classic', 'dazzle'),  
					'minimal-menu' => esc_html__('Minimal', 'dazzle'), 
					'fullscreen-menu' => esc_html__('Fullscreen', 'dazzle')
					),
				'std' 		  => 'classic-menu',
				'multiple'    => false,
			),	

			array(
				'name'        => esc_html__( 'Pick a new menu?', 'dazzle' ),
				'id'          => "{$dazzle_prefix}page_new_menu",
				'type'        => 'select_advanced',
				'hidden' 	  => array('dazzle_pagenav_behavior_switch', '!=', '1'),
				'desc'		  => esc_html__('Select a new menu for the header.','dazzle'),
				'options' => $dazzle_menu_array,
				'multiple'    => false,
			),		

			array(
				'name'        => esc_html__( 'Initial Navigation Style', 'dazzle' ),
				'id'          => "{$dazzle_prefix}initial_navigation_style",
				'type'        => 'select_advanced',
				'columns'	  => '6',
				'hidden' => array('dazzle_pagenav_behavior_switch', '!=', '1'),
				'options'     => array(
						'light-header' => esc_html__( 'Light Background / Dark Navigation', 'dazzle' ),
						'dark-header' => esc_html__( 'Dark Background / Light Navigation', 'dazzle' ),
					),
				'std' 		  => 'light-header',
				'multiple'    => false,
			),
			
			array(
				'name'        => esc_html__( 'On Scroll Navigation Style', 'dazzle' ),
				'id'          => "{$dazzle_prefix}onscroll_navigation_style",
				'hidden' => array('dazzle_pagenav_behavior_switch', '!=', '1'),
				'type'        => 'select_advanced',
				'columns'	  => '6',
				'options'     => array(
						'light-header' => esc_html__( 'Light Background / Dark Navigation', 'dazzle' ),
						'dark-header' => esc_html__( 'Dark Background / Light Navigation', 'dazzle' ),
					),
				'std' 		  => 'light-header',
				'multiple'    => false,
			),	

			array(
				'name' => esc_html__( 'Initial Header Background Color', 'dazzle' ),
				'id'   => "{$dazzle_prefix}initial_header_color",
				'type' => 'color',
				'columns'	  => '6',
				'hidden' => array('dazzle_pagenav_behavior_switch', '!=', '1'),
				'std'  => '#ffffff'
			),	

			array(
				'name' => esc_html__( 'On Scroll Header Background Color', 'dazzle' ),
				'id'   => "{$dazzle_prefix}onscroll_header_color",
				'type' => 'color',
				'hidden' => array('dazzle_pagenav_behavior_switch', '!=', '1'),
				'columns'	  => '6',
				'std'  => '#ffffff'
			),			

			array(
				'name'       => esc_html__( 'Initial Header Background Color Opacity', 'dazzle' ),
				'id'         => "{$dazzle_prefix}initial_header_color_opacity",
				'type'       => 'slider',
				'hidden' => array('dazzle_pagenav_behavior_switch', '!=', '1'),
				'columns'	  => '6',				
				'suffix'     => esc_html__( '%', 'dazzle' ),
				'js_options' => array(
					'min'  => 0,
					'max'  => 100,
					'step' => 1,
					'value' => 100
				),
			),		
			array(
				'name'       => esc_html__( 'On Scroll Header Background Color Opacity', 'dazzle' ),
				'id'         => "{$dazzle_prefix}onscroll_header_color_opacity",
				'type'       => 'slider',
				'hidden' => array('dazzle_pagenav_behavior_switch', '!=', '1'),
				'columns'	  => '6',				
				'suffix'     => esc_html__( '%', 'dazzle' ),
				'js_options' => array(
					'min'  => 0,
					'max'  => 100,
					'step' => 1,
					'value'=> 100
				),
			),		

			array(
				'name' => esc_html__( 'Initial Logo Image(Optional)', 'dazzle' ),
				'id'   => "{$dazzle_prefix}initial_logo_image",
				'hidden' => array('dazzle_pagenav_behavior_switch', '!=', '1'),
				'columns'	  => '6',						
				'type'             => 'image_advanced',
				'max_status'	   => false,
				'max_file_uploads' => 1,
			),		
			array(
				'name' => esc_html__( 'On Scroll Logo Image(Optional)', 'dazzle' ),
				'id'   => "{$dazzle_prefix}onscroll_logo_image",
				'hidden' => array('dazzle_pagenav_behavior_switch', '!=', '1'),
				'columns'	  => '6',						
				'type'             => 'image_advanced',
				'max_status'	   => false,
				'max_file_uploads' => 1,
			),
			array(
				'name' => esc_html__( 'SVG or Retina Ready?', 'dazzle' ),
				'id'   => "{$dazzle_prefix}initial_logo_svg_retina",
				'desc'  => esc_html__( 'If your logo is an .svg file or retina-ready .png file, set a width and height for it.', 'dazzle' ),
				'columns'	  => '6',	
				'type' => 'checkbox',
				'hidden' => array('dazzle_pagenav_behavior_switch', '!=', '1'),
				'std'  => 0,
			),
			array(
				'name' => esc_html__( 'SVG or Retina Ready?', 'dazzle' ),
				'id'   => "{$dazzle_prefix}onscroll_logo_svg_retina",
				'desc'  => esc_html__( 'If your logo is an .svg file or retina-ready .png file, set a width and height for it.', 'dazzle' ),
				'columns'	  => '6',	
				'type' => 'checkbox',
				'hidden' => array('dazzle_pagenav_behavior_switch', '!=', '1'),
				'std'  => 0,
			),			
			array(
				'name'  => esc_html__( 'Initial Logo Width(px)', 'dazzle' ),
				'id'    => "{$dazzle_prefix}initial_svg_retina_logo_width",
				'type'  => 'number',
				'columns'	  => '6',	
				'hidden' => array('dazzle_initial_logo_svg_retina', '!=', '1'),
				'size'	=> 50,
			),	
			array(
				'name'  => esc_html__( 'OnScroll Logo Width(px)', 'dazzle' ),
				'id'    => "{$dazzle_prefix}onscroll_svg_retina_logo_width",
				'hidden' => array('dazzle_onscroll_logo_svg_retina', '!=', '1'),
				'type'  => 'number',
				'columns'	  => '6',	
				'size'	=> 50,
			),	
			array(
				'name'  => esc_html__( 'Initial Logo Height(px)', 'dazzle' ),
				'id'    => "{$dazzle_prefix}initial_svg_retina_logo_height",
				'type'  => 'number',
				'columns'	  => '6',	
				'hidden' => array('dazzle_initial_logo_svg_retina', '!=', '1'),
				'size'	=> 50,
			),	
			array(
				'name'  => esc_html__( 'OnScroll Logo Height(px)', 'dazzle' ),
				'id'    => "{$dazzle_prefix}onscroll_svg_retina_logo_height",
				'hidden' => array('dazzle_onscroll_logo_svg_retina', '!=', '1'),
				'type'  => 'number',
				'columns'	  => '6',	
				'size'	=> 50,
			),																										
		),
	);	

	$dazzle_meta_boxes[] = array(
		'id'         => 'dazzle_blog_post_tagline_metaboxes',
		'title'      => esc_html__( 'Blog Post Options', 'dazzle' ),
		'post_types' => array( 'post' ),
		'context'    => 'normal',
		'priority'   => 'low',
		'autosave'   => true,
		'fields'     => array(
			array(
				'name'  => esc_html__( 'Blog Post Title Tagline', 'dazzle' ),
				'id'    => "{$dazzle_prefix}blog_post_tagline",
				'desc'  => esc_html__( 'You can set a tagline for the blog post title(optional). It will appear on single blog post pages.', 'dazzle' ),
				'type'  => 'text',
				'size'	=> 50,
			),	
		),
	);


	$dazzle_meta_boxes[] = array(
		'id'         => 'dazzle_portfolio_thumbnail_metaboxes',
		'title'      => esc_html__( 'Featured Image / Thumbnail Options', 'dazzle' ),
		'post_types' => array( 'portfolio' ),
		'context'    => 'side',
		'priority'   => 'low',
		'autosave'   => true,
		'fields'     => array(
			array(
				'name'        => esc_html__( 'Thumbnail Behavior', 'dazzle' ),
				'id'          => "{$dazzle_prefix}portf_icon",
				'type'        => 'select_advanced',
				'options'     => array(
					'link_to_page'	=> esc_html__( 'Opens the Project Page', 'dazzle' ),
					'lightbox_to_image'	=> esc_html__( 'Is Opening in a Lightbox', 'dazzle' ),
					'link_to_link'	=> esc_html__( 'Opens a Custom URL', 'dazzle' ),
					'lightbox_to_video'	=> esc_html__( 'Opens a Video in a Lightbox', 'dazzle' ),
					'lightbox_to_gallery'	=> esc_html__( 'Opens an Image Gallery in a Lightbox', 'dazzle' ),
					),
				'multiple'    => false,
			),		

			array(
				'name'  => esc_html__( 'Custom URL: ', 'dazzle' ),
				'id'    => "{$dazzle_prefix}portf_link",
				'desc'  => esc_html__( 'You can set the thumbnail to open a custom URL.', 'dazzle' ),
				'type'  => 'text',
				'hidden' => array( 'dazzle_portf_icon', '!=', 'link_to_link' ),
			),

			array(
				'name'  => esc_html__( 'Video URL: ', 'dazzle' ),
				'id'    => "{$dazzle_prefix}portf_video",
				'desc'  => esc_html__( 'You can set the thumbnail to open a video from third-party websites(Vimeo, YouTube) in an URL. Ex: http://www.youtube.com/watch?v=y6Sxv-sUYtM', 'dazzle' ),
				'type'  => 'text',
				'hidden' => array( 'dazzle_portf_icon', '!=', 'lightbox_to_video' ),
			),

			array(
				'name'             => esc_html__( 'Gallery Images', 'dazzle' ),
				'id'               => "{$dazzle_prefix}portf_gallery",
				'type'             => 'image_advanced',
				'max_status'	   => false,
				'max_file_uploads' => 30,
				'desc'			   => esc_html__( 'Upload new images or select them from the media library. (Ctrl/CMD + Click for selecting multiple items at once)', 'dazzle' ),
				'hidden' => array( 'dazzle_portf_icon', '!=', 'lightbox_to_gallery' ),
			),				

			array(
				'name'    => esc_html__( 'Thumbnail Size', 'dazzle' ),
				'id'      => "{$dazzle_prefix}portf_thumbnail",
				'type'    => 'image_select',
				'options' => array(
					'small' => $dazzle_img_dir_path.'portfolio-small.png',
					'wide' => $dazzle_img_dir_path.'portfolio-big.png',
					'horizontal' => $dazzle_img_dir_path.'half-horizontal.png',
					'vertical' => $dazzle_img_dir_path.'half-vertical.png',
				),
				'std'	=> 'small',					
			),

			array(
				'name' => esc_html__( 'Set a .GIF file as thumbnail', 'dazzle' ),
				'id'   => "{$dazzle_prefix}set_project_thumbnail_gif",
				'desc'  => esc_html__( 'Display a .gif file instead of the current thumbnail/featured image.', 'dazzle' ),
				'type' => 'checkbox',
				'std'  => 0,
			),	
			array(
				'name'        => esc_html__( '.GIF URL', 'dazzle' ),
				'id'          => "{$dazzle_prefix}gif_url",
				'type'        => 'file_advanced',
				'hidden' => array('dazzle_set_project_thumbnail_gif', '!=', '1'),
			),						
		),
	);		


	$dazzle_meta_boxes[] = array(
		'id'         => 'dazzle_project_description_metabox',
		'title'      => esc_html__( 'Project Options', 'dazzle' ),
		'post_types' => array( 'portfolio' ),
		'context'    => 'normal',
		'priority'   => 'high',
		'autosave'   => true,
		'fields'     => array(

			array(
				'name'  => esc_html__( 'Small Project Description', 'dazzle' ),
				'id'    => "{$dazzle_prefix}small_pr_description",
				'desc'  => esc_html__( 'Add a small description for the project. It will be applied to certain portfolio styles.(HTML tags allowed)', 'dazzle' ),
				'type'  => 'textarea',
			),	
		),
	);		
	
	return $dazzle_meta_boxes;

}
?>