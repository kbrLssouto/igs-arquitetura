<?php
/*

Template Name: Blog Template

 */

$dazzle_blog_layout = get_post_meta($post->ID, 'dazzle_blog_layout', true);

$dazzle_layout_class = '';
$dazzle_sidebar_class = '';
$dazzle_column_class = '';

$dazzle_sidebar = array('regular-right', 'regular-left', 'masonry-2-cols-sr', 'masonry-2-cols-sl');

switch($dazzle_blog_layout) {
	case 'regular-right':
		$dazzle_sidebar_class = 'sidebar-right percent-blog';
	break;

	case 'regular-left':
		$dazzle_sidebar_class = 'sidebar-left percent-blog';
	break;

	case 'regular':
		$dazzle_sidebar_class = 'no-sidebar';
	break;

	case 'masonry-3-cols':
		$dazzle_layout_class = 'blog-masonry';
		$dazzle_sidebar_class = 'no-sidebar';
		$dazzle_column_class = 'on-three-columns';
	break;

	case 'masonry-2-cols':
		$dazzle_layout_class = 'blog-masonry';
		$dazzle_sidebar_class = 'no-sidebar';
		$dazzle_column_class = 'on-two-columns';
	break;

	case 'masonry-2-cols-sr':
		$dazzle_layout_class = 'blog-masonry';
		$dazzle_sidebar_class = 'sidebar-right percent-blog';
		$dazzle_column_class = 'on-two-columns';
	break;

	case 'masonry-2-cols-sl':
		$dazzle_layout_class = 'blog-masonry';
		$dazzle_sidebar_class = 'sidebar-left percent-blog';
		$dazzle_column_class = 'on-two-columns';
	break;

		
}

?>

<?php get_header(); ?>

<?php get_template_part( 'inc/page-title' ); ?>

	<?php
		if (have_posts()) : while (have_posts()) : the_post(); ?>

		<?php the_content(); ?>		

	<?php endwhile; ?>

	<?php endif;
	 ?>		
	
	<div class="container">

	<?php

		if ( get_query_var('paged') ) {
		    $paged = get_query_var('paged');
		} elseif ( get_query_var('page') ) {
		    $paged = get_query_var('page');
		} else {
		    $paged = 1;
		}		

		$dazzle_args = array(
			'post_type'=> 'post',
			'paged'=>$paged
		);	

		$dazzle_blog_query = new WP_Query($dazzle_args);
	?>

	<div id="primary" class="content-area <?php echo esc_attr($dazzle_sidebar_class); ?>">
		<main id="main" class="site-main <?php echo esc_attr($dazzle_layout_class).' '.esc_attr($dazzle_column_class); ?>">
			<?php if ($dazzle_layout_class == 'blog-masonry') { ?>
				<div class="grid-content">
					<div class="gutter-sizer"></div>
			<?php } ?>
				
			<?php 
			if ($dazzle_blog_query->have_posts()) :  while ($dazzle_blog_query->have_posts()) : $dazzle_blog_query->the_post(); 

				if ($dazzle_layout_class != 'blog-masonry') {
					get_template_part( 'template-parts/content', get_post_format() );
				} else {
					get_template_part( 'template-parts/content', 'blog-shortcode' );
				}	
				endwhile;
			endif;  
			?>	
				<div class="clear"></div>
			<?php if ($dazzle_layout_class == 'blog-masonry') { ?>	
				</div><!-- .grid-content -->
			<?php } ?> 

			<?php dazzle_navigation(); ?>
			
			<?php wp_reset_postdata(); ?>

		</main><!-- #main -->
		
	</div><!-- #primary -->

	<?php if (in_array($dazzle_blog_layout, $dazzle_sidebar)) { 
		get_sidebar(); 
	} ?>

	<div class="space"></div>
</div><!--end container-->

<?php get_footer();
