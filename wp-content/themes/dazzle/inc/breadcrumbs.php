<?php
/** Breadcrumbs */

function dazzle_breadcrumbs() {

    $dazzle_data = dazzle_dt_data();
 
    $dazzle_text['home']     = esc_html__('Home', 'dazzle'); 
    $dazzle_text['category'] = '%s'; 
    $dazzle_text['search']   = "%s";
    $dazzle_text['tag']      = "%s";
    $dazzle_text['author']   = '%s'; 
    $dazzle_text['404']      = esc_html__('Error 404', 'dazzle'); 
 
    $dazzle_show_current   = 1; 
    $dazzle_show_on_home   = 0;
    $dazzle_show_home_link = 1;
    $dazzle_show_title     = 1;
    $dazzle_delimiter      = ' <i class="fa fa-angle-right"></i> '; 
    $dazzle_before         = '<span class="current">';
    $dazzle_after          = '</span>';
 
    global $post;
    $dazzle_home_link    = home_url('/');
    $dazzle_link_before  = '';
    $dazzle_link_after   = '';
    $dazzle_link_attr    = '';
    $dazzle_link         = $dazzle_link_before . '<a' . $dazzle_link_attr . ' href="%1$s">%2$s</a>' . $dazzle_link_after;
    $dazzle_parent_id    = $dazzle_parent_id_2 = isset($post) ? $post->post_parent : 0;
    $dazzle_frontpage_id = get_option('page_on_front');

    $dazzle_blog_keyword = esc_html__('Blog', 'dazzle'); 
    $dazzle_portfolio_keyword = $dazzle_blog_keyword = esc_html__('Portfolio', 'dazzle'); 
    if(!empty($dazzle_data['dazzle_breadcrumbs_blog_keyword'])) {
        $dazzle_blog_keyword = $dazzle_data['dazzle_breadcrumbs_blog_keyword'];
    }
    if(!empty($dazzle_data['dazzle_breadcrumbs_portfolio_keyword'])) {
        $dazzle_portfolio_keyword = $dazzle_data['dazzle_breadcrumbs_portfolio_keyword'];
    }    
 
    if (is_home() || is_front_page()) {
 
        if ($dazzle_show_on_home == 1) echo '<span class="current">' . $dazzle_text['home'] . '</span>';
 
    } else {
 
        if ($dazzle_show_home_link == 1) {
            echo sprintf($dazzle_link, $dazzle_home_link, $dazzle_text['home']);
            if ($dazzle_frontpage_id == 0 || $dazzle_parent_id != $dazzle_frontpage_id) echo $dazzle_delimiter;
        }
 
        if ( is_category() ) {
            $dazzle_this_cat = get_category(get_query_var('cat'), false);
            if ($dazzle_this_cat->parent != 0) {
                $dazzle_cats = get_category_parents($dazzle_this_cat->parent, TRUE, $dazzle_delimiter);
                if ($dazzle_show_current == 0) $dazzle_cats = preg_replace("#^(.+)$dazzle_delimiter$#", "$1", $dazzle_cats);
                $dazzle_cats = str_replace('<a', $dazzle_link_before . '<a' . $dazzle_link_attr, $dazzle_cats);
                $dazzle_cats = str_replace('</a>', '</a>' . $dazzle_link_after, $dazzle_cats);
                if ($dazzle_show_title == 0) $dazzle_cats = preg_replace('/ title="(.*?)"/', '', $dazzle_cats);
                echo $dazzle_cats;
            }
            if ($dazzle_show_current == 1) echo $dazzle_before . sprintf($dazzle_text['category'], single_cat_title('', false)) . $dazzle_after;
 
        } elseif ( is_search() ) {
            echo $dazzle_before . sprintf($dazzle_text['search'], get_search_query()) . $dazzle_after;
 
        } elseif ( is_day() ) {
            echo sprintf($dazzle_link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $dazzle_delimiter;
            echo sprintf($dazzle_link, get_month_link(get_the_time('Y'),get_the_time('m')), get_the_time('F')) . $dazzle_delimiter;
            echo $dazzle_before . get_the_time('d') . $dazzle_after;
 
        } elseif ( is_month() ) {
            echo sprintf($dazzle_link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $dazzle_delimiter;
            echo $dazzle_before . get_the_time('F') . $dazzle_after;
 
        } elseif ( is_year() ) {
            echo $dazzle_before . get_the_time('Y') . $dazzle_after;
 
        } elseif ( is_single() && !is_attachment() ) {
             if ( get_post_type() == 'post' ) {
                if(!empty($dazzle_data['dazzle_breadcrumbs_blog_url'])) {
                    echo '<a href="' . esc_url($dazzle_data['dazzle_breadcrumbs_blog_url']) . '">' . $dazzle_blog_keyword . '</a> ' . $dazzle_delimiter . ' ';
                }
                echo $dazzle_before . get_the_title() . $dazzle_after;
            }
            else
            if ( get_post_type() == 'portfolio' ) {
                if(!empty($dazzle_data['dazzle_breadcrumbs_blog_url'])) {
                    echo '<a href="' . esc_url($dazzle_data['dazzle_breadcrumbs_portfolio_url']) . '">' . $dazzle_portfolio_keyword . '</a> ' . $dazzle_delimiter . ' ';
                }
                echo $dazzle_before . get_the_title() . $dazzle_after;
            }   

             else {
                $dazzle_cat = get_the_category(); $dazzle_cat = $dazzle_cat[0];
                if ($dazzle_cat->cat_ID != 1) { //Не показывать категорию "Без рубрики"
                    $dazzle_cats = get_category_parents($dazzle_cat, TRUE, $dazzle_delimiter);
                    if ($dazzle_show_current == 0) $dazzle_cats = preg_replace("#^(.+)$dazzle_delimiter$#", "$1", $dazzle_cats);
                    $dazzle_cats = str_replace('<a', $dazzle_link_before . '<a' . $dazzle_link_attr, $dazzle_cats);
                    $dazzle_cats = str_replace('</a>', '</a>' . $dazzle_link_after, $dazzle_cats);
                    if ($dazzle_show_title == 0) $dazzle_cats = preg_replace('/ title="(.*?)"/', '', $dazzle_cats);
                    echo $dazzle_cats;
                }
                if ($dazzle_show_current == 1) echo $dazzle_before . get_the_title() . $dazzle_after;
            }
 
        } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
            $dazzle_post_type = get_post_type_object(get_post_type());
            echo $dazzle_before . $dazzle_post_type->labels->singular_name . $dazzle_after;
 
        } elseif ( is_attachment() ) {
            $dazzle_parent = get_post($dazzle_parent_id);
            $dazzle_cat = get_the_category($dazzle_parent->ID); $dazzle_cat = $dazzle_cat[0];
            $dazzle_cats = get_category_parents($dazzle_cat, TRUE, $dazzle_delimiter);
            $dazzle_cats = str_replace('<a', $dazzle_link_before . '<a' . $dazzle_link_attr, $dazzle_cats);
            $dazzle_cats = str_replace('</a>', '</a>' . $dazzle_link_after, $dazzle_cats);
            if ($dazzle_show_title == 0) $dazzle_cats = preg_replace('/ title="(.*?)"/', '', $dazzle_cats);
            echo $dazzle_cats;
            printf($dazzle_link, get_permalink($dazzle_parent), $dazzle_parent->post_title);
            if ($dazzle_show_current == 1) echo $dazzle_delimiter . $dazzle_before . get_the_title() . $dazzle_after;
 
        } elseif ( is_page() && !$dazzle_parent_id ) {
            if ($dazzle_show_current == 1) echo $dazzle_before . get_the_title() . $dazzle_after;
 
        } elseif ( is_page() && $dazzle_parent_id ) {
            if ($dazzle_parent_id != $dazzle_frontpage_id) {
                $dazzle_breadcrumbs = array();
                while ($dazzle_parent_id) {
                    $page = get_page($dazzle_parent_id);
                    if ($dazzle_parent_id != $dazzle_frontpage_id) {
                        $dazzle_breadcrumbs[] = sprintf($dazzle_link, get_permalink($page->ID), get_the_title($page->ID));
                    }
                    $dazzle_parent_id = $page->post_parent;
                }
                $dazzle_breadcrumbs = array_reverse($dazzle_breadcrumbs);
                for ($dazzle_i = 0; $dazzle_i < count($dazzle_breadcrumbs); $dazzle_i++) {
                    echo $dazzle_breadcrumbs[$dazzle_i];
                    if ($dazzle_i != count($dazzle_breadcrumbs)-1) echo $dazzle_delimiter;
                }
            }
            if ($dazzle_show_current == 1) {
                if ($dazzle_show_home_link == 1 || ($dazzle_parent_id_2 != 0 && $dazzle_parent_id_2 != $dazzle_frontpage_id)) echo $dazzle_delimiter;
                echo $dazzle_before . get_the_title() . $dazzle_after;
            }
 
        } elseif ( is_tag() ) {
            echo $dazzle_before . sprintf($dazzle_text['tag'], single_tag_title('', false)) . $dazzle_after;
 
        } elseif ( is_author() ) {
            global $author;
            $dazzle_userdata = get_userdata($author);
            echo $dazzle_before . sprintf($dazzle_text['author'], $dazzle_userdata->display_name) . $dazzle_after;
 
        } elseif ( is_404() ) {
            echo $dazzle_before . $dazzle_text['404'] . $dazzle_after;
        }
 
        if ( get_query_var('paged') ) {
            if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
            echo esc_html__('Page', 'dazzle') . ' ' . get_query_var('paged');
            if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
        }
 
    }
}
?>