<?php
/*

Posts Navigation Function

*/
function dazzle_navigation($dazzle_pages = '', $dazzle_range = 2)
{
	$dazzle_show_items = ($dazzle_range * 2)+1; 

	global $paged;
	if(empty($paged)) $paged = 1;

	if($dazzle_pages == '')
	{
		global $wp_query, $dazzle_blog_query;
		if (is_page_template('template-blog.php')) {
			$dazzle_pages = $dazzle_blog_query->max_num_pages;
		}
		else $dazzle_pages = $wp_query->max_num_pages;
		if(!$dazzle_pages)
		{
		 $dazzle_pages = 1;
		}
	}  

	if(1 != $dazzle_pages)
		{
		echo "<div class=\"pagenav\">";
		if($paged > 2 && $paged > $dazzle_range+1 && $dazzle_show_items < $dazzle_pages) echo "<a href='".esc_url(get_pagenum_link(1))."'>".esc_html__('&laquo;', 'dazzle')." ".esc_html__('First', 'dazzle')."</a>";
		if($paged > 1 && $dazzle_show_items < $dazzle_pages) echo "<a href='".esc_url(get_pagenum_link($paged - 1))."'>".esc_html__('&lsaquo;', 'dazzle')." ".esc_html__('Previous', 'dazzle')."</a>";

		for ($i=1; $i <= $dazzle_pages; $i++)
		{
			if (1 != $dazzle_pages &&( !($i >= $paged+$dazzle_range+1 || $i <= $paged-$dazzle_range-1) || $dazzle_pages <= $dazzle_show_items ))
				{
					 echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".esc_url(get_pagenum_link($i))."' class=\"inactive\">".$i."</a>";
				}
		}

		if ($paged < $dazzle_pages && $dazzle_show_items < $dazzle_pages) echo "<a href=\"".esc_url(get_pagenum_link($paged + 1))."\">".esc_html__('&rsaquo;', 'dazzle')."</a>";
		if ($paged < $dazzle_pages-1 &&  $paged+$dazzle_range-1 < $dazzle_pages && $dazzle_show_items < $dazzle_pages) echo "<a href='".esc_url(get_pagenum_link($dazzle_pages))."'>".esc_html__('&raquo;', 'dazzle')."</a>";
		echo "</div>\n";
		}
	}
?>