<?php
/**
 * Page title section for this theme
 *
 * @package Dazzle Theme
 */
if((!is_404()) || (!is_search() ) ) { 
	$dazzle_tagline = get_post_meta($post->ID, 'dazzle_page_tagline', true);
	$dazzle_title = get_post_meta($post->ID, 'dazzle_page_title', true);
	$dazzle_position = get_post_meta($post->ID, 'dazzle_page_title_position', true);
	$dazzle_blog_post_title = get_post_meta($post->ID, 'dazzle_blog_post_tagline', true);
}
?>
<?php if($dazzle_title != '1') { ?>
	<div class="page-title-wrapper">
		<div class="container <?php echo esc_attr($dazzle_position); ?>">
		<?php
			if(is_home()) { ?>
				<div class="nine columns flexbase">
					<h1><?php esc_html_e("Blog", "dazzle"); ?></h1>
					<?php
					if(!empty($dazzle_tagline)) {
					 	echo '<h4>'. esc_html($dazzle_tagline) . '</h4>';
					} ?>
				</div>
		
				<div class="three columns flexbase">
					<?php get_search_form(); ?>
				</div>				
			<?php }
			else if (is_archive()) { 
				if (have_posts()) : 
					$dazzle_post = $posts[0];				
					the_archive_title( '<h1>', '</h1>' );
					the_archive_description( '<h4>', '</h4>' ); 
				endif; 					
			}
			else if (is_page_template('template-blog.php')) { ?>	
				<div class="nine columns flexbase">
					<?php echo '<h1>'. esc_html(get_the_title()) . '</h1>'; 
					if(!empty($dazzle_tagline)) {
					 	echo '<h4>'. esc_html($dazzle_tagline) . '</h4>';
					} ?>
				</div>
		
				<div class="three columns flexbase">
					<?php get_search_form(); ?>
				</div>
			<?php }
			else if (is_page()) {
				 echo '<h1>'. esc_html(get_the_title()) . '</h1>'; 
				if(!empty($dazzle_tagline)) {
				 	echo '<h4>'. esc_html($dazzle_tagline) . '</h4>';
				}
			} 	
			else if ('portfolio' == get_post_type()) {
				 echo '<h1>'. esc_html(get_the_title()) . '</h1>'; 
				if(!empty($dazzle_tagline)) {
				 	echo '<h4>'. esc_html($dazzle_tagline) . '</h4>';
				}				
			}					
			else if (is_single()) {
				echo '<h1>'. wp_kses_post(get_the_title()) . '</h1>'; 
				if(isset($dazzle_blog_post_title)) {
					if($dazzle_blog_post_title != '') {
						echo '<h3 class="delicious-post-title">'.wp_kses_post($dazzle_blog_post_title).'</h3>';
					}
				}
				echo '<header class="entry-header"><div class="entry-meta">';
				dazzle_posted_on();
				echo '</div></header>';
			}
			?>

		</div>
	</div>	

<?php get_template_part( 'inc/page-title-breadcrumbs' ); ?>

	<div class="space under-title"></div>

<?php } ?> 