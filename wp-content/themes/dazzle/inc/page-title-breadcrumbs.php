<?php 
$dazzle_data = dazzle_dt_data();

if((!is_404()) || (!is_search() ) ) { 
	$dazzle_position = get_post_meta($post->ID, 'dazzle_page_title_position', true);
}

if (is_singular('portfolio')) {
	if(($dazzle_data['dazzle_breadcrumbs_enabled'] == 1) && ($dazzle_data['dazzle_breadcrumbs_for']['projects'] == 1)) { ?>
		<div class="container for-breadcrumbs <?php echo esc_attr($dazzle_position); ?>">
			<div class="dt-breadcrumbs">
				<?php dazzle_breadcrumbs(); ?>
			</div>
		</div>
	<?php } 
} else if (is_singular() || is_page_template('template-blog.php')) {
	if(($dazzle_data['dazzle_breadcrumbs_enabled'] == 1) && ($dazzle_data['dazzle_breadcrumbs_for']['posts'] == 1)) { ?>
		<div class="container for-breadcrumbs <?php echo esc_attr($dazzle_position); ?>">
			<div class="dt-breadcrumbs">
				<?php dazzle_breadcrumbs(); ?>
			</div>
		</div>
	<?php } } else if (is_page()) {
	if(($dazzle_data['dazzle_breadcrumbs_enabled'] == 1) && ($dazzle_data['dazzle_breadcrumbs_for']['pages'] == 1)) { ?>
		<div class="container for-breadcrumbs <?php echo esc_attr($dazzle_position); ?>">
			<div class="dt-breadcrumbs">
				<?php dazzle_breadcrumbs(); ?>
			</div>
		</div>
	<?php } }